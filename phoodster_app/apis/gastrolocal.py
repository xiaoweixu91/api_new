# -*- coding: utf-8 -*-
__author__ = 'xuxiaowei'
from phoodster_app import app
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import request, jsonify
import time
from elasticsearch import ElasticsearchException
from sqlalchemy import and_, or_
import tinys3, StringIO
import uuid

api = Namespace('', description='Gastrolocals related operations')


########################gastrolocal recipes#################################################

@api.route('/gastrolocal_recipes')
class GastroRecipes(Resource):
    @api.doc('gastrolocal recipes')
    @use_args(GasRecipesSchema())
    def get(self, args):
        """

        :param args: gastrolocalID or token
        :return:
        """
        gastrolocalID = args["gastrolocalID"]
        if not gastrolocalID:
            token = request.args.get("token")
            result = blogger_information.query.filter_by(token=token).first()
            if result:
                gastrolocalID=result.BloggerId
            else:
                return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})




        bloggerinfo = blogger_information.query.filter_by(BloggerId=gastrolocalID).first()
        bloggerinfo, errors = blogger_information_instance.dump(bloggerinfo)

        query = {
            "size": 10,
            "_source": {
                "include": ["ingredients", "name", "cook_time", "prep_time", "servings",
                            "short_description", "uniqid", "num_ingredients", "credit_text", "steps"]
            },

            "query": {
                "match": {
                    "token": gastrolocalID
                }
            }
        }
        try:
            recipes = es.search(index='sweden_recipes_upload_recipe', doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})
        # print bloggerinfo
        return jsonify({"status": "OK", "bloggerinfo": bloggerinfo, "recipes": recipes['hits']['hits']})



########################check token#################################################

@api.route('/check_token',methods=['POST'])
class CheckToken(Resource):
    @api.doc('check token')
    @use_args({'token': fields.Str()})
    def post(self, args):
        token = args['token']

        result = blogger_information.query.filter_by(token=token).first()
        if result:
            # print 'token is right'
            return jsonify({"status": 1, "message": "Token correct"})
        else:
            return jsonify({"status": 0, "message": "Token incorrect"})


########################upload recipe#################################################
@api.route('/publishnewrecipe', methods=['POST'])
@api.route('/gastrolocal_recipe', methods=['POST'])
class GastrolocalRecipe(Resource):
    @api.doc('upload recipe')
    @use_args(UploadRecipeSchema())
    def post(self, args):
        blogger_info = blogger_information.query.filter_by(token=args['token']).first()
        new_ingredients = []
        for i in args['ingredients'][0]['ingredients']:
            new_ingredients.append(
                {'name': i['name'], 'measure': i['quantity'] + ' ' + i['unit'], 'quantity': i['quantity'],
                 'unit': i['unit']})
        #
        args['ingredients'] = [{"dish": "", "ingredients": new_ingredients}]
        args['source_url'] = ''
        data_name = args['uniqid']
        data_name = data_name.lower().replace(u'å', u'a').replace(u'ä', u'a').replace(u'ö', u'o').replace(u"ü",
                                                                                                          u"u").replace(
            u"è", u"e").replace(u"é", u"e").replace(u"ê", u"e")  #
        args['uniqid'] = data_name.replace(' ', '_')
        args['images'] = [{
            "url": "https://" + app.config['AWS_ENDPOINT'] + "/gastrolocals-data/recipe-images/" + args['uniqid'],
            "checksum": "",
            "path": ""
        }]
        args['image_urls'] = []
        # args['categories'] = [i.strip() for i in re.split(',',args['tag'])]
        args['categories'] = args['tag']
        args['credit_text'] = blogger_info.BloggerName
        args['token'] = blogger_info.BloggerId

        query={
            "query":{
                "match":{
                    "uniqid":args['uniqid']
                }
            }
        }


        exists=es.search_exists(index='sweden_recipes_upload_recipe',doc_type='recipe',body=query)
        #check if uniqid is unique
        if exists:
            args['uniqid']=args['uniqid']+str(random.randint(0,9))

        data = args
        # upload to es

        try:
            es.index(index='sweden_recipes_upload_recipe', doc_type="recipe", body=data)
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})


        #### AWS
        conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True,
                                 endpoint=app.config['AWS_ENDPOINT'])
        output = StringIO.StringIO()
        output.write(data)

        conn.upload("recipes/" + data_name + '.json', output, bucket='gastrolocals-data', public=True)

        output.close()

        return jsonify({"status": "OK"})

    @api.doc('update')
    @use_args(UploadRecipeSchema())
    def update(self,args):


        blogger_info = blogger_information.query.filter_by(token=args['token']).first()

        new_ingredients = []
        for i in args['ingredients'][0]['ingredients']:
            new_ingredients.append(
                {'name': i['name'], 'measure': i['quantity'] + ' ' + i['unit'], 'quantity': i['quantity'],
                 'unit': i['unit']})

        args['ingredients'] = [{"dish": "", "ingredients": new_ingredients}]
        args['source_url'] = ''
        data_name = args['uniqid']
        data_name = data_name.lower().replace(u'å', u'a').replace(u'ä', u'a').replace(u'ö', u'o').replace(u"ü",
                                                                                                          u"u").replace(
            u"è", u"e").replace(u"é", u"e").replace(u"ê", u"e")  #
        args['uniqid'] = data_name.replace(' ', '_')
        args['images'] = [{
            "url": "https://" + app.config['AWS_ENDPOINT'] + "/gastrolocals-data/recipe-images/" + args['uniqid'],
            "checksum": "",
            "path": ""
        }]
        args['image_urls'] = []

        if blogger_info:
            args['credit_text'] = blogger_info.BloggerName
            args['token'] = blogger_info.BloggerId

        try:
            es.update(index='sweden_recipes_upload_recipe', doc_type="recipe", id=arg['id'], body={"doc": args})
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})

        #### AWS
        conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True, endpoint=app.config['AWS_ENDPOINT'])
        output = StringIO.StringIO()
        output.write(args)

        conn.upload("recipes/" + data_name + "_" + str(uuid.uuid1()) + '.json', output, bucket='gastrolocals-data',
                    public=True)

        output.close()

        return jsonify({"status": "OK", "message": "Recipe updated"})

########################gastrolocals image#################################################
@api.route('/upload_image', methods=['POST'])
@api.route('/gastrolocal_image', methods=['POST'])
class GastrolocalImage(Resource):
    @api.doc('upload image')
    def post(self):
        data = request.files.getlist('userpic[]')[0]
        name = request.files.getlist('userpic[]')[0].filename
        #### AWS
        conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True,
                                 endpoint=app.config['AWS_ENDPOINT'])
        conn.upload("recipe-images/" + name, data, bucket='gastrolocals-data', public=True, content_type='image')

        return jsonify({"status": "OK"})

########################gastrolocals#################################################
@api.route('/fetch_gastrolocals', methods=['GET'])
@api.route('/gastrolocal', methods=['GET'])
class Gastrolocals(Resource):
    @api.doc('Gastrolocals info')
    def get(self):
        bloggers = blogger_information.query.all()
        bloggers, errors = bloggers_information_instance.dump(bloggers)
        return jsonify({'status': 'OK', 'result': bloggers})
