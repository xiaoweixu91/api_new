__author__ = 'xuxiaowei'
from phoodster_app import app
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import request, jsonify, send_file
import time, io

from flask_mail import Message
from elasticsearch import ElasticsearchException
from sqlalchemy import and_, or_
import tinys3, StringIO, requests
from werkzeug.security import check_password_hash, generate_password_hash

api = Namespace('', description='User related operations')


########################edit profile#################################################
# @api.route('/edit_profile', methods=['POST'])  # old
@api.route('/profile', methods=['POST','DELETE'])  # new
class Profile(Resource):
    @api.doc('profile')
    @use_args(ProfileSchema())
    def post(self, args):
        """

        :param args: full_name,bio,username,password,photo_path,token
        :return:
        """
        full_name = args['full_name']
        bio = args['bio']
        username = args['username']
        password = args['password']
        photo_path = args['photo_path']
        try:
            user_id = get_user_id()
        except Exception as error:
            return jsonify(status='INVALID_REQUEST', message=error.message)
        filename = ''

        if 'photo' in request.files:
            photo = request.files['photo']
            if photo and photo.filename != '' and allowed_file(photo.filename):

                extension = photo.filename.rsplit('.', 1)[1]
                filename = get_random_string(12) + '.' + extension
                conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True,
                                         endpoint=app.config['AWS_ENDPOINT'])
                conn.upload(filename, photo, 'phoodster-user-images', public=True)
                remove_user_photo(user_id)


        elif request.form.get('delete_photo') == 'delete_photo':

            remove_user_photo(user_id)
        else:
            filename = request.form.get('photo_path')


        if username != None:
            if internal_check_username_available(username) is True:
                the_user=users.query.filter_by(id=user_id).first()
                the_user.username = username
                db.session.commit()


        if password:

            password_hash = generate_password_hash(password)
            user = users.query.filter_by(id=user_id).first()
            user.photo_path = filename
            user.full_name = full_name
            user.bio = bio
            user.password_hash = password_hash

        else:
            user = users.query.filter_by(id=user_id).first()
            user.photo_path = filename
            user.full_name = full_name
            user.bio = bio

        db.session.commit()

        user = users.query.filter_by(id=user_id).first()
        user,error = user_instance.dump(user)

        return jsonify(status='OK', message='Profile saved', photo_path=filename, user=user)
    @api.doc('delete user and all profile')
    def delete(self):

        try:
            user_id = get_user_id()
        except Exception as error:
            return jsonify(status='INVALID_REQUEST', message=error.message)

        users.query.filter_by(id=user_id).delete()

        # cursor.execute('DELETE FROM user_preference WHERE id=%s', (user_id,))
        # mysql.connection.commit()

        results=db.session.query(shopping_lists,ingredients,recipes_in_list).\
            outerjoin(ingredients,ingredients.id==shopping_lists.id).\
            outerjoin(recipes_in_list,recipes_in_list.shopping_list_id==shopping_lists.id).\
            filter(shopping_lists.user_id==user_id).all()
        for result in results:
            db.session.delete(result)
        favorite_stores.query.filter_by(user_id=user_id).delete()

        _results = db.session.query(recipes).\
            outerjoin(decks,decks.id==recipes.deck_id).\
            filter(decks.user_id==user_id).\
            all()
        for _result in _results:
            db.session.delete(_result)

        db.session.commit()


        return 'successfully deleted'




########################send feedback#################################################

@api.route('/send_feedback', methods=['POST'])
class Feedback(Resource):
    @api.doc('Send Feedback')
    @use_args(FeedbackSchema())
    def post(self, args):
        """

        :param args: message,token
        :return:
        """

        message = args['message']
        if message is None or message == ':':
            return jsonify(status='INVALID_REQUEST', message='The message is needed')

        user_id = None
        try:
            user_id = get_user_id()
        except Exception as error:
            pass
            # return jsonify(status='INVALID_REQUEST', message='User not identified')

        body = ''

        if user_id:
            user = users.query.filter_by(id=user_id).first()

            if user:
                body += 'Username: ' + user.username + '\n'
                body += 'Name: ' + user.full_name + '\n'
                body += 'Email: ' + user.email + '\n'

        body += message
        message = Message("Feedback",
                          body=body,
                          sender='no-reply@phoodster.com',
                          recipients=['sanna.bengtsson@phoodster.com'],
                          bcc=['christoph.peters@phoodster.com'])
        mail.send(message)

        return jsonify(status='OK', message='Thank you for the feedback!')


########################user photo#################################################

@api.route('/users_photos/<filename>')
class userPhoto(Resource):
    @api.doc('user photo')
    def get(self, filename):
        url = 'https://s3-eu-west-1.amazonaws.com/phoodster-user-images/' + filename
        response = requests.get(url, stream=True)
        return send_file(io.BytesIO(response.raw.read()), attachment_filename=filename, mimetype='image/png')


########################user data with token#################################################

@api.route('/user_data_with_token', methods=['GET'])
class userDateWithToken(Resource):
    @api.doc('user data with token')
    def get(self):
        try:
            user_id = get_user_id()
        except Exception as error:
            return jsonify(status='INVALID_REQUEST', message=error.message)

        the_user = users.query.filter_by(id=user_id).first()
        the_user,error = user_instance.dump(the_user)
        if the_user:
            return jsonify(status='OK', user=the_user)

        return jsonify(status='NOT_FOUND', message='The user/email and/or password does not match with any user')


@api.route('/check_username_available')
class AvailableUsername(Resource):
    @api.doc('check username available')
    @use_args({'old_username':fields.Str(),'new_username':fields.Str()})
    def get(self,args):
        new_username = args["new_username"]
        old_username = args["old_username"]

        if new_username != old_username:
            try:
                if not internal_check_username_available(new_username) :
                    return jsonify({"status": "not available", "message": "username is not available."})
                else:
                    return jsonify({"status": "available", "message": "username is available."})

            except:
                jsonify({"status": "failed", "message": "Database access failed."})
        else:
            return jsonify({"status": "not changed", "message": "Username did not change."})



