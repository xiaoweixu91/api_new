# -*- coding: utf-8 -*-
__author__ = 'xuxiaowei'
from phoodster_app import app
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import request, jsonify
import time
from elasticsearch import ElasticsearchException
from sqlalchemy import and_, or_

api = Namespace('', description='Stores/Offers related operations')


########################Get Stores#################################################
@api.route('/stores')  # new
class Stores(Resource):
    @api.doc('get_stores')
    @use_args(StoresSchema())
    def get(self, args):
        """

        :param args: lat,lng,q,token,page
        :param per_page: status,results,paging
        :return:
        """
        lat = args['lat']
        lng = args['lng']
        q   = args['q']
        page = args['page']
        size = args['size']
        if not lat or not lng:
            return jsonify(status='INVALID_REQUEST',
                           message='Both Latitude and Longitude must be supplied as parameters.')

        db.session.add(store_search_location(date=time.strftime('%Y-%m-%d'), latitude=lat, longitude=lng))
        db.session.commit()


        show_items = 'showitems' in request.args

        offers_index = app.config['OFFER_INDEX_MAP'].get('sv', 'sweden_offers')

        if not args['q']:
            match_q = {"match_all": {}}
        else:

            match_q = {
                "multi_match": {
                    "query": q,
                    "fields": ["name^2", "name.grams", "address"],
                    "type": "cross_fields"
                }
            }

        query = {
            "query": {
                "function_score": {
                    "query": {"filtered": {"filter": {"and": [{"range": {"offer_count": {"gte": 0}}}, {
                        "geo_distance": {"distance": "20km", "location": {"lat": args['lat'], "lon": args['lng']}}}]},
                                           "query": match_q}},
                    "linear": {
                        "location": {"origin": {"lat": args['lat'], "lon": lng}, "scale": "30km",
                                     "decay": 0.9}}}},
            "from": (page - 1) * size, "size": size,
            "_source": {"include": ["address", "name", "postal_code", "uuid", "offer_count", "brand", "location"]},
            "script_fields": {
                "distance": {"params": {"lat": lat, "lon": lng},
                             "script": "doc['location'].arcDistance(lat,lon)"}}
        }
        if show_items:
            query['_source']['include'].append('items')

        try:
            stores = es.search(index=offers_index, doc_type="offer", body=query)
        except ElasticsearchException as err:
            return jsonify({"status": "SERVER_ERROR", 'message': str(err)})

        results = []
        for s in stores['hits']['hits']:
            source = s['_source']
            source['id'] = s['_id']
            source['distance'] = round(s['fields']['distance'][0])
            results.append(source)

        # check if the store is favorite or not
        user_id=''
        if args['token']:
            user = users.query.filter_by(token=args['token']).first()
            user_id = user.id
            if user_id:
                for i in results:
                    result = favorite_stores.query.filter(
                        and_(favorite_stores.user_id == user_id, favorite_stores.uuid == i['uuid'])).count()

                    if result:
                        i['favorite'] = True
                    else:
                        i['favorite'] = False

        total = stores['hits']['total']
        paging = pager(total, args['page'], size, 100)
        return jsonify({"status": "OK", "results": results, "paging": paging})


########################STORE OFFERS#################################################

@api.route('/store_offers')
class Offers(Resource):
    @api.doc('fetch offers')
    @use_args(OffersSchema())
    def get(self, args):
        """

        :param args: country,uuid
        :return:status,results
        """
        offers_index = app.config['OFFER_INDEX_MAP'].get(args['country'], 'sweden_offers')
        query = {
            "filter": {
                "term": {
                    "uuid": args['uuid']
                }
            },
            "_source": {
                "include": "items"
            }
        }

        try:
            offers = es.search(index=offers_index, doc_type="offer", body=query)
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error on Server"})

        results = []
        for item in offers["hits"]["hits"][0]["_source"]["items"]:
            results.append(item)

        return jsonify({"status": "OK", "results": results})


########################store detail#################################################
@api.route('/store_detail') #new
class StoreDetail(Resource):
    @api.doc('get store detail')
    @use_args(StoreSchema())
    def get(self, args):
        """

        :param args: uuid, lat, lng, country, token
        :return:
        """

        uuid = args['uuid']
        lat = args['lat']
        lng = args['lng']
        country = args['country']
        token = args['token']

        offers_index = app.config['OFFER_INDEX_MAP'].get(country, 'sweden_offers')

        if lat and lng:

            query = {
                "filter": {
                    "term": {
                        "uuid": uuid
                    }
                },
                "_source": {
                    "include": ["address", "name", "postal_code", "uuid", "offer_count", "brand"]
                },
                "script_fields": {
                    "distance": {"params": {"lat": float(lat), "lon": float(lng)}, "script": "doc['location'].arcDistance(lat,lon)"}}
            }
        else:
            query = {
                "filter": {
                    "term": {
                        "uuid": uuid
                    }
                },
                "_source": {
                    "include": ["address", "name", "postal_code", "uuid", "offer_count", "brand"]
                }
            }

        try:
            offers = es.search(index=offers_index, doc_type="offer", body=query)
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error on Server"})

        if lat and lng:
            offers["hits"]["hits"][0]["_source"]["distance"] = round(offers["hits"]["hits"][0]['fields']['distance'][0])
        else:
            offers["hits"]["hits"][0]["_source"]["distance"] = "NA"
        offers['hits']['hits'][0]['_source']['id']=offers['hits']['hits'][0]['_id']
        offer = offers['hits']['hits']

        if token != '' and token != 'undefined' and token != None:

            user = users.query.filter_by(token=token).first()
            user_id = user.id
            if user_id:
                result = favorite_stores.query.filter(
                    and_(favorite_stores.user_id == user_id, favorite_stores.uuid == uuid)).count()
                if result:
                    offer[0]['_source']['favorite'] = True
                else:
                    offer[0]['_source']['favorite'] = False


        return jsonify({"status": "OK", "results": offer})
