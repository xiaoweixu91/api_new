__author__ = 'xuxiaowei'

from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import request, jsonify
import time
from elasticsearch import ElasticsearchException
from sqlalchemy import and_, or_

api = Namespace('', description='Recipes related operations')


########################################################################
def recipes_at_category_params(category, country, uuid, page, per_page):
    """
    :param category:
    :param country:
    :param uuid:

    TODO: Validate: if category is set

    :return:
    """
    tags_result = tags.query.filter(
        and_(tags.category_id == category, tags.country == country)).all()

    tags_name = [item.name for item in tags_result]
    tags_joined = ','.join(tags_name)

    if not tags_result and not uuid:
        return jsonify(status="INVALID_REQUEST", message="Missing document id parameter or tag list")
    offers_index = app.config['OFFER_INDEX_MAP'].get(country, 'sweden_offers')

    # Add offers
    if country == 'sv':
        analyzer = 'search_grams'
    else:
        analyzer = 'english_analyzer'

    if uuid:
        id_q = {
            'filter': {
                "term": {
                    "uuid": uuid
                }
            }
        }

        try:
            result = es.search(index=offers_index, doc_type="offer", body=id_q)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

        try:
            store = result['hits']['hits'][0]
        except IndexError:
            return jsonify({'status': 'INVALID_REQUEST', 'message': 'Document with the id doesn\'t Exist'})
        else:
            store_items = store['_source']['items']
    else:
        store_items = []



    # offers = [prepare_nested_query(removeSpecialCharacters(item['title'].encode('utf-8')), _, analyzer) for _, item in enumerate(store_items)]
    offers = [prepare_nested_query(item['title'], _, analyzer) for _, item in enumerate(store_items)]

    query = {
        "size": per_page,
        "from": (page - 1) * per_page,
        "_source": {
            "include": ["course", "credit_text", "extra_info", "images", "ingredients", "name", "prep_time", "servings",
                        "source_url", "special_diet", "uniqid", "categories", "num_ingredients"]
        },
        "query": {
            "bool": {
                "must": {
                    "match": {"categories": tags_joined}
                }
            }
        }
        # ,
        # "sort": {
        #     "_script" : {
        #         "script": "Math.random()",
        #         "type": "number",
        #         "params": {},
        #         "order": "asc"
        #     }
        # }
    }
    # minimum match depends on the number of offers
    if offers:
        query["query"]["bool"]["should"] = offers
        if len(offers) > 20:
            query["query"]["bool"]["minimum_should_match"] = 1
        else:
            query["query"]["bool"]["minimum_should_match"] = 1

    recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, "sweden_recipes")

    try:
        recipes = es.search(index=recipes_index, doc_type="recipe", body=query)
    except ElasticsearchException:
        return {"status": "SERVER_ERROR", "message": "Error On Server."}

    total = recipes['hits']['total']
    paging = pager(total, page, per_page, 100)
    items = recipes['hits']['hits']
    random.shuffle(items, random.random)


    # TODO: Probably must create store_items
    response = process_response(store_items, items)

    return response, paging


########################################################################



#######################Get Random Recipes#################################################

@api.route('/random_recipes')
class RandomRecipes(Resource):
    @api.doc('get_random_recipes')
    @use_args(RecipesSchema())
    def get(self, args):
        """

        :param args: q,page,country,size
        :return: status,items, paging, q, catid
        """

        q = args['q']
        country = args['country']
        page = args['page']
        size = args['size']
        query_index = app.config['RECIPE_INDEX_MAP'].get(country, 'sweden_recipes')

        exclude = request.args.get('exclude', '')
        exclude_params = exclude.split(',')

        if not q:
            match_q = {'match_all': {}}
        else:

            match_q = {"multi_match": {"query": q,
                                       "fields":
                                           ["name^10", "name.grams^2", "special_diet^5", "short_description",
                                            "extra_info", "cuisine", "course"
                                            ],
                                       "operator": "and",
                                       "analyzer": "standard", "type": "cross_fields"}}

        negative_queries = []
        for _ in exclude_params:
            n_q = prepare_negative_query(_)
            if n_q:
                negative_queries.append(n_q)

        if negative_queries:
            match_q = {'bool': {'must': [match_q] + negative_queries}}

        query = {
            "query": {
                "function_score": {
                    'query': match_q,
                    "functions": [
                        {
                            "random_score": {
                                "seed": int(time.time())
                            }
                        }
                    ]
                },

            },
            "from": (page - 1) * size,
            "size": size
        }

        try:
            recipes = es.search(index=query_index, doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

        total = recipes['hits']['total']
        paging = pager(total, page, size, 100)
        return jsonify(status='OK', items=recipes['hits']['hits'], paging=paging, q=q, catid='')


########################recipes_for_category#################################################

@api.route('/recipes_category')
class CategoryRecipes(Resource):
    @api.doc('get recipes in category')
    @use_args(RecipesSchema())
    def get(self, args):
        """

        :param args: category,country,
        :return: status,id,payload
        """
        category = args['category']
        country = args['country']
        page = args['page']
        per_page = args['size']
        try:
            tags_result = tags.query.filter(
                and_(tags.category_id == category, tags.country == country)).all()
            tags_name = [item.name for item in tags_result]
            tags_joined = ','.join(tags_name)
        except Exception as error:
            return jsonify(status='INVALID_REQUEST', message=error.message)
        query = {
            "size": per_page,
            "from": (page - 1) * per_page,
            "_source": {
                "include": ["course", "credit_text", "extra_info", "images", "ingredients", "name", "prep_time",
                            "servings",
                            "source_url", "special_diet", "uniqid", "categories", "num_ingredients", "rating"]
            },

            "query": {
                "function_score": {
                    "query": {
                        "bool": {
                            "should": [
                                {
                                    "match": {
                                        "categories": tags_joined
                                    }
                                }
                            ]
                        }

                    },
                    "script_score": {
                        "script": "_score * doc['rating'].value"
                    }

                }
            }

        }

        recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, "sweden_recipes")



        try:
            recipes = es.search(index=recipes_index, doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})
        total = recipes['hits']['total']

        paging = pager(total, page, per_page, 100)

        # TODO: Probably must create store_items
        response = recipes  # process_response(store_items, recipes["hits"]["hits"])

        return jsonify({"status": "OK", "id": "_id","paging":paging, "q":"","catid":category,"items": response["hits"]["hits"]})


#######################RECIPE#################################################

@api.route('/recipe', methods=['GET', 'POST'])
class Recipe(Resource):
    @api.doc('fetch recipe')
    @use_args(RecipeSchema())
    def get(self, args):
        """

        :param args: uniqid, token, guest_id, country
        :return: status, recipe
        """
        uniqid = args['uniqid']
        token = args['token']
        guest_id = args['guest_id']
        country = args['country']
        # The user_id is optional
        if token:
            user_id = get_user_id()
        else:
            user_id = None

        if not uniqid:
            return jsonify({'status': 'INVALID_REQUEST', 'message': 'Missing uniqid parameter.'})

        recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, "sweden_recipes")

        query = {
            "_source": {
                "include": ["course", "credit_text", "short_description", "images", "ingredients", "name", "prep_time",
                            "servings",
                            "source_url", "special_diet", "uniqid", "num_ingredients", "steps", "token"]
            },
            "query": {
                "match": {"uniqid": uniqid}
            }
        }

        try:
            result = es.search(index=recipes_index, doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})

        items = result["hits"]["hits"]

        if len(items) == 0:
            return jsonify({"status": "INVALID_REQUEST", "message": "The recipe was not found"})

        item = result["hits"]["hits"][0]
        item["favorite"] = False
        item["_source"]["index"] = item["_index"]
        item["_source"]["id"] = item["_id"]

        if user_id:
            recipe_uniqid = item["_source"]["uniqid"]
            result = db.session.query(recipes). \
                outerjoin(decks, recipes.deck_id == decks.id). \
                outerjoin(users, decks.user_id == users.id). \
                filter(and_(users.id == user_id, recipes.uniqid == recipe_uniqid)). \
                count()

            if result is not None:
                item["favorite"] = True
        elif guest_id:

            recipe_uniqid = item["_source"]["uniqid"]
            result = db.session.query(recipes). \
                outerjoin(decks, recipes.deck_id == decks.id). \
                outerjoin(users, decks.user_id == users.id). \
                filter(and_(users.id == guest_id, recipes.uniqid == recipe_uniqid)). \
                count()
            if result is not None:
                item["favorite"] = True

        return jsonify({"status": "OK", "recipe": item})


########################recipes_category_store#################################################
@api.route('/recipes_at_category')  # old
@api.route('/recipes_category_store')  # new
class RecipesCategoryStore(Resource):
    @api.doc('get recipes in category for store')
    @use_args(RecipesSchema())
    def get(self, args):
        """

        :param args: category,country,page,uuid,per_page
        :return:
        """
        category = args["category"]
        country = args["country"]
        page = args['page']
        uuid = args["uuid"]
        per_page = args['size']

        try:
            response, paging = recipes_at_category_params(category, country, uuid, page, per_page)
            res = {"status": "OK", "id": uuid, "items": response, "paging": paging, "q": "", "catid": category}

            return jsonify(res)
        except Exception:
            return jsonify({"status": "OK", "id": uuid, "items": [], "paging": 1, "q": "", "catid": category})


########################recipes__store#################################################
@api.route('/cook_detail')  # old
@api.route('/recipes_store')  # new
class RecipesStore(Resource):
    @api.doc('get recipes for stores')
    @use_args(RecipesSchema())
    def get(self, args):
        """

        :param args: uuid, q, country, page, size
        :return:
        """

        uuid = args['uuid']
        q = args['q']
        country = args['country']
        page = args['page']
        per_page = args['size']
        offers_index = app.config['OFFER_INDEX_MAP'].get(country, 'sweden_offers')

        exclude = request.args.get('exclude', '')
        exclude_params = exclude.split(',')

        if not uuid:
            return jsonify({'status': 'INVALID_REQUEST', 'message': 'Missing document id parameter.'})

        id_q = {
            'filter': {
                "term": {
                    "uuid": uuid
                }
            }
        }

        try:
            result = es.search(index=offers_index, doc_type="offer", body=id_q)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

        try:
            store = result['hits']['hits'][0]
        except IndexError:
            return jsonify({'status': 'INVALID_REQUEST', 'message': 'Document with the id doesn\'t Exist'})

        recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, 'sweden_recipes')
        if country == 'sv':
            analyzer = 'search_grams'
        else:
            analyzer = 'english_analyzer'

        store_items = store['_source']['items']
        offers = [prepare_nested_query(item['title'], _, analyzer) for _, item in enumerate(store_items)]

        if not q:
            match_q = {'match_all': {}}
        else:
            match_q = {"multi_match": {"query": q,
                                       "fields": ["name^10", "name.grams^2", "special_diet^5", "short_description",
                                                  "extra_info", "cuisine", "course", "categories"], "operator": "and",
                                       "analyzer": "standard", "type": "cross_fields"}}

        negative_queries = []
        for _ in exclude_params:
            n_q = prepare_negative_query(_)
            if n_q:
                negative_queries.append(n_q)

        if negative_queries:
            match_q = {'bool': {'must': [match_q] + negative_queries}}

        query = {
            "query":
                {"bool":
                    {
                        "must": match_q,
                        # "should": offers,"minimum_should_match": 1
                        "should": offers, "minimum_number_should_match": 1}

                },
            "_source": {
                "include": ["course", "credit_text", "extra_info", "images", "ingredients", "name", "prep_time",
                            "servings", "source_url", "special_diet", "uniqid", "num_ingredients"]},
            "from": (page - 1) * per_page,
            "size": per_page,

        }

        try:
            recipes = es.search(index=recipes_index, doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})
        try:
            response = process_response(store_items, recipes['hits']['hits'])
            total = recipes['hits']['total']
            paging = pager(total, page, per_page, 100)
            res = {"status": 'OK', "uuid": uuid, "items": response, "paging": paging, "q": q, "catid": ""}
            return jsonify(res)
        except Exception:
            return jsonify({"status": 'OK', "uuid": uuid, "items": [], "paging": 1, "q": q, "catid": ""})


########################recipe for store#################################################
# @api.route('/get_recipe_for_store')  # old
@api.route('/recipe_store')  # new
class RecipeStore(Resource):
    @api.doc('get recipe for store')
    @use_args(StoreRecipeSchema())
    def get(self, args):
        """

        :param args: storeID,recipeID,country
        :return:
        """
        storeID = args['storeID']
        recipeID = args['recipeID']
        country = args['country']
        offers_index = app.config['OFFER_INDEX_MAP'].get(country, 'sweden_offers')

        exclude = request.args.get('exclude', '')
        exclude_params = exclude.split(',')

        if not storeID:
            return jsonify({'status': 'INVALID_REQUEST', 'message': 'Missing document id parameter.'})

        id_q = {
            'filter': {
                "term": {
                    "uuid": storeID
                }
            }
        }

        try:
            result = es.search(index=offers_index, doc_type="offer", body=id_q)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

        try:
            store = result['hits']['hits'][0]
        except IndexError:
            return jsonify({'status': 'INVALID_REQUEST', 'message': 'Document with the id doesn\'t Exist'})

        recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, 'sweden_recipes')
        if country == 'sv':
            analyzer = 'search_grams'
            # analyzer = 'edge_gram'

        store_items = store['_source']['items']
        offers = [prepare_nested_query(item['title'], _, analyzer) for _, item in enumerate(store_items)]

        query = {
            "query": {"bool": {"should": offers, "minimum_should_match": 0, "must": {"match": {"uniqid": recipeID}}}},
            "_source": {
                "include": ["course", "credit_text", "extra_info", "images", "ingredients", "name", "prep_time",
                            "servings", "source_url", "special_diet", "uniqid", "num_ingredients"]},
            "size": 30}

        try:
            recipes = es.search(index=recipes_index, doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})
        response = process_response(store_items, recipes['hits']['hits'])

        res = {"status": 'OK', "storeid": storeID, "recipeid": recipeID, "payload": response}
        return jsonify(res)


########################similar recipes#################################################

@api.route('/similar_recipes')
class SimilarRecipes(Resource):
    @api.doc('similar recipes')
    @use_args({'search_index':fields.Str(),'recipeID':fields.Str()})
    def get(self, args):
        """

        :param args:  search_index, recipeID
        :return:
        """
        search_index = args['search_index']
        recipeID = args['recipeID']
        query = {
            "query": {
                "more_like_this": {
                    "fields": ["name", "short_description", "categories"],
                    "docs": [
                        {
                            "_index": search_index,
                            "_type": "recipe",
                            "_id": recipeID
                        }],
                    "min_term_freq": 1,
                    "max_query_terms": 12,
                    "stop_words": ["med"],
                    "analyzer": "search_grams"
                }
            },
            "size": 6
        }
        try:
            recipes = es.search(index="sweden_recipes", doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

        return jsonify({"status": "OK", "results": recipes['hits']['hits']})


########################recipes at shopping list#################################################

@api.route('/recipes_at_shopping_list', methods=['GET'])
class RecipeAtShoppingList(Resource):
    @api.doc('recipes at shopping list')
    @use_args({'id': fields.Str()})
    def get(self, args):
        _id = args['id']

        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        recipes = recipes_in_list.query.filter_by(shopping_list_id=_id).all()

        uniqids = [item.recipe_uniqid for item in recipes]
        recipes_indexes = ','.join(app.config['RECIPE_INDEX_MAP'].itervalues())
        query = {
            "query": {
                "constant_score": {
                    "filter": {
                        "terms": {
                            "uniqid": uniqids
                        }
                    }
                }
            }
        }
        results = es.search(index=recipes_indexes, doc_type='recipe', body=query)

        return jsonify(status='OK', recipes=results['hits']['hits'])


@api.route('/recipe_at_deck', methods=['DELETE','POST'])
class RecipeAtDeck(Resource):
    @api.doc('recipes at deck')
    @use_args({'user_id': fields.Str(), 'guest_id': fields.Str(), 'id': fields.Str()})
    def delete(self, args):
        user_id = None
        guest_id = None


        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        _id = args['id']

        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        if user_id:
            results = db.session.query(recipes). \
                outerjoin(decks, recipes.deck_id == decks.id). \
                outerjoin(users, decks.user_id == users.id). \
                filter(and_(recipes.id == _id, users.id == user_id)). \
                all()
            for result in results:
                db.session.delete(result)

        elif guest_id:
            results = db.session.query(recipes). \
                outerjoin(decks, recipes.deck_id == decks.id). \
                filter(and_(recipes.id == _id, decks.guest_id == guest_id)). \
                all()
            for result in results:
                db.session.delete(result)
        else:
            return jsonify(status='INVALID_REQUEST', message=INVALID_SESSION_MESSAGE)

        db.session.commit()

        return jsonify(status='OK', message='Recipe deleted')

    @api.doc('create new recipe at deck')
    @use_args(DeckRecipeSchema())
    def post(self, args):
        user_id = None
        guest_id = None
        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        uniqid = args['uniqid']
        if not uniqid:
            return jsonify(status='INVALID_REQUEST', message='The uniqid is required')

        name = args['name']
        credit = args['credit']
        image_path = args['image_path']
        image_source_url = args['image_source_url']

        deck_id = args['deck_id']
        deck_name = args['deck_name']
        deck_description = args['deck_description']

        country = args['country']

        if not name:
            return jsonify(status='INVALID_REQUEST', message='The name is required')


        # TODO: Validate if the deck is owned by the user or guest
        if not deck_id:
            deck_name = incremental_table_name('decks', 'Deck', deck_name, user_id, guest_id)

            if user_id:
                new_deck = decks(name=deck_name, description=deck_description, user_id=user_id)
                db.session.add(new_deck)
            elif guest_id:
                new_deck = decks(name=deck_name, description=deck_description, guest_id=guest_id)
                db.session.add(new_deck)
            else:
                return jsonify(status='INVALID_REQUEST', message=INVALID_SESSION_MESSAGE)
            db.session.commit()
            deck_id = new_deck.id

        new_recipe = recipes(name=name, credit=credit, image_path=image_path, image_source_url=image_source_url,
                             deck_id=deck_id, uniqid=uniqid, country=country)
        db.session.add(new_recipe)
        db.session.commit()

        return jsonify(status='OK', recipe_id=new_recipe.id)


@api.route('/cook by ingredients',methods=['GET'])
class CookByIngredients(Resource):
    @api.doc('cook by ingredients')
    @use_args({'ingredients':fields.List(fields.Str()),'id':fields.Str(),'country':fields.Str(missing='sv')})
    def get(self,args):
        """

        :return:
        """
        ingredients = args['ingredients']
        _id = args['id']

        if not ingredients and not _id:
            return jsonify(status='INVALID_REQUEST', message='Missing document id parameter or ingredients list')

        country = args['country']
        offers_index = app.config['OFFER_INDEX_MAP'].get(country, 'sweden_offers')

        if country == 'sv':
            analyzer = 'search_grams'
        else:
            analyzer = 'english_analyzer'

        if _id:
            id_q = {
                'filter': {
                    "term": {
                        "_id": _id
                    }
                }
            }

            try:
                result = es.search(index=offers_index, doc_type="offer", body=id_q)
            except ElasticsearchException:
                return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

            try:
                store = result['hits']['hits'][0]
            except IndexError:
                return jsonify({'status': 'INVALID_REQUEST', 'message': 'Document with the id doesn\'t Exist'})
            else:
                store_items = store['_source']['items']
        else:
            store_items = []

        offers = [prepare_nested_query(item['title'], _, analyzer) for _, item in enumerate(store_items)]

        query = {'query': {'bool': {}}, "_source": {
            "include": ["course", "credit_text", "extra_info", "images", "ingredients", "name", "prep_time",
                        "servings", "source_url", "special_diet", "uniqid", "num_ingredients"]},
                 "size": 50}

        if ingredients:
            store_item_length = len(offers)

            match_q = {"nested": {"path": "ingredients.ingredients", "score_mode": "avg", "query": {"match": {
                "ingredients.ingredients.name": {"query": ingredients, "cutoff_frequency": 0.001, "operator": "and",
                                                 "analyzer": analyzer}}},
                                  "inner_hits": {"name": store_item_length}}}

            query['query']['bool']['must'] = match_q

        if offers:
            query['query']['bool']['should'] = offers
            query['query']['bool']['minimum_should_match'] = 1

        recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, 'sweden_recipes')

        try:
            recipes = es.search(index=recipes_index, doc_type="recipe", body=query)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

        response = process_response(store_items, recipes['hits']['hits'])

        res = {"status": 'OK', "id": _id, "payload": response}
        return jsonify(res)


@api.route('/favorite_recipes_uniqids', methods=['GET'])
class FavoriteRecipesUniqids(Resource):
    @api.doc('favorite recipe uniqids')
    @use_args({'guest_id':fields.Str()})
    def get(self,args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = request.form.get('guest_id')

        if user_id:
            results=db.session.query(recipes).\
            outerjoin(decks,decks.id==recipes.deck_id).\
            outerjoin(users,decks.user_id==users.id).\
            filter(users.id==user_id).\
            all()

        elif guest_id:
            results=db.session.query(recipes).\
            outerjoin(decks,decks.id==recipes.deck_id).\
            filter(decks.guest_id==guest_id).\
            all()

        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)

        uniqids= [result.uniqid for result in results]

        return jsonify(status='OK', uniqids=uniqids)



