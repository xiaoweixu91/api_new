__author__ = 'xuxiaowei'
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import jsonify
import json
from sqlalchemy import and_, or_, func
from elasticsearch import ElasticsearchException
from flask_cors import cross_origin
api = Namespace('', description='Ingredient related operations')


@api.route('/ingredient', methods=['POST', 'DELETE','PUT'])
class Ingredient(Resource):
    @api.doc('create new ingredient')
    @use_args(IngredientSchema())
    def post(self, args):
        name = args['name']
        description = args['description']
        measure = args['measure']
        price = args['price']

        shopping_list_id = args['shopping_list_id']
        shopping_list_name = args['shopping_list_name']
        shopping_list_description = args['shopping_list_description']

        recipe_uniqid = args['recipe_uniqid']

        if not name:
            return jsonify(status='INVALID_REQUEST', message='The name is required')

        user_id = None
        guest_id = None
        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        # TODO: Validate if the shopping_list is owned by the user

        if not shopping_list_id:
            shopping_list_name = incremental_table_name('shopping_lists', 'List', shopping_list_name, user_id, guest_id)

            if user_id:
                new_shopping_list = shopping_lists(name=shopping_list_name, description=shopping_list_description,
                                                   user_id=user_id)
                db.session.add(new_shopping_list)

            elif guest_id:
                new_shopping_list = shopping_lists(name=shopping_list_name, description=shopping_list_description,
                                                   guest_id=guest_id)
                db.session.add(new_shopping_list)
            else:
                return jsonify(status='INVALID_REQUEST', message=INVALID_SESSION_MESSAGE)
            db.session.commit()
            shopping_list_id = new_shopping_list.id
        if recipe_uniqid :
            new_recipes_in_list = recipes_in_list(shopping_list_id=shopping_list_id, recipe_uniqid=recipe_uniqid)
            db.session.add(new_recipes_in_list)

        new_ingredient = ingredients(name=name, description=description, measure=measure, price=price,
                                     shopping_list_id=shopping_list_id,done=0)
        db.session.add(new_ingredient)
        db.session.commit()

        return jsonify(status='OK', ingredient_id=new_ingredient.id)

    @api.doc('done ingredient')
    @use_args({'id': fields.Str(), 'done': fields.Boolean()})
    def put(self, args):
        _id = args['id']
        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        done = args['done']
        if not done:
            return jsonify(status='INVALID_REQUEST', message='The done is required')

        # TODO: Validate if the user is the owner
        the_ingredient = ingredients.query.filter_by(id=_id).first()
        the_ingredient.done = done
        db.session.commit()
        # cursor.execute('UPDATE ingredients SET done = %s WHERE id = %s', (done, _id,))
        # mysql.connection.commit()
        return jsonify(status='OK', message='Success')

    @api.doc('delete ingredient')
    @use_args({'id': fields.Str()})
    def delete(self, args):
        _id = args['id']
        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        # TODO: Validate if the user is the owner
        ingredients.query.filter_by(id=_id).delete()
        db.session.commit()
        return jsonify(status='OK', message='Ingredient deleted')


#################################################################################

@api.route('/ingredients', methods=['POST', 'DELETE', 'PUT'])
class Ingredients(Resource):
    @api.doc('save new ingredients')
    def post(self):
        json_params = request.get_json()

        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = json_params.get('guest_id')

        shopping_list_id = json_params.get('shopping_list_id')
        shopping_list_name = json_params.get('shopping_list_name', None)
        shopping_list_description = json_params.get('shopping_list_description', None)

        recipe_uniqid = json_params.get('recipe_uniqid', None)

        # TODO: Validate the shopping_list is owned by the current user

        if not shopping_list_id:
            shopping_list_name = incremental_table_name('shopping_lists', 'List', shopping_list_name, user_id, guest_id)

            if user_id:

                new_shopping_list = shopping_lists(user_id=user_id, name=shopping_list_name,
                                                   description=shopping_list_description)

            elif guest_id:
                new_shopping_list = shopping_lists(guest_id=guest_id, name=shopping_list_name,
                                                   description=shopping_list_description)

            else:
                return jsonify(status='INVALID_REQUEST', message=INVALID_SESSION_MESSAGE)
            db.session.add(new_shopping_list)
            db.session.commit()
            shopping_list_id = new_shopping_list.id

        if recipe_uniqid:
            new_recipes_in_list = recipes_in_list(shopping_list_id=shopping_list_id, recipe_uniqid=recipe_uniqid)
            db.session.add(new_recipes_in_list)
            db.session.commit()

        _ingredients = json_params['ingredients']

        if len(_ingredients) == 0:
            return jsonify(status='INVALID_REQUEST', message='0 ingredients was received')
        for _ingredient in _ingredients:
            new_ingredient = ingredients(name=_ingredient['name'], description=_ingredient['description'],
                                         measure=_ingredient['measure'], price=_ingredient['price'],
                                         shopping_list_id=shopping_list_id,
                                         done=0
                                         )
            db.session.add(new_ingredient)
            db.session.commit()

        return jsonify(status='OK', shopping_list_id=shopping_list_id)

    @api.doc('delete ingredients')
    @use_args({'ids': fields.List(fields.Str())})
    def delete(self, args):

        _ids = args['ids']

        if len(_ids) == 0:
            return jsonify(status='INVALID_REQUEST', message='0 ingredients was received')

        try:
            for _id in _ids:
                # TODO: Validate if the user is the owner
                ingredients.query.filter_by(id=_id).delete()
                db.session.commit()
                # cursor.execute('DELETE FROM ingredients WHERE id = %s', (_id,))
                # mysql.connection.commit()
        except:
            return jsonify(status='FAILED', message='Ingredients not deleted')

        return jsonify(status='OK', message='Ingredients deleted')




#######################most used ingredients##########################################################

@api.route('/fetch_most_used_ingredients')
@api.route('/most_used_ingredients')
class MostUsedIngredients(Resource):
    @api.doc('fetch_most_used_ingredients')
    def get(self):
        result = most_used_ingredients.query.all()
        result = most_used_ingredients_instance.dump(result)
        return jsonify({"status": "OK", "result": result})
