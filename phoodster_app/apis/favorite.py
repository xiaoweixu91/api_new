__author__ = 'xuxiaowei'

from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import jsonify

from sqlalchemy import and_, or_

api = Namespace('', description='Favorite store related operations')


@api.route('/favorites', methods=['GET', 'POST', 'DELETE'])
class Favorite(Resource):
    @api.doc('add favorite store')
    @use_args(FavoriteSchema())
    def post(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        _id = args['id']
        name = args['name']
        address = args['address']
        uuid = args['uuid']
        location = args['location']

        if not uuid:
            return jsonify(status='INVALID_REQUEST', message='The uuid is required')

        if user_id:
            new_favorite_store = favorite_stores(id=_id, name=name, address=address, uuid=uuid, user_id=user_id,
                                                 location=location, guest_id=None)
        elif guest_id:
            new_favorite_store = favorite_stores(id=_id, name=name, address=address, uuid=uuid, user_id=None,
                                                 location=location, guest_id=guest_id)
        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)
        try:
            db.session.add(new_favorite_store)
            db.session.commit()
        except Exception:
            return jsonify(status='FAIL', message='Already exists')

        store_id = new_favorite_store.internal_id

        if store_id:
            return jsonify(status='OK', favorite_store_id=store_id)
        else:
            return jsonify(status='SERVER_ERROR', message='Something went wrong')

    @api.doc('remove favorite store')
    @use_args(FavoriteSchema())
    def delete(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        uuid = args['uuid']
        if not uuid:
            return jsonify(status='INVALID_REQUEST', message='The uuid is required')

        if user_id:
            favorite_stores.query.filter(
                and_(favorite_stores.user_id == user_id, favorite_stores.uuid == uuid)).delete()

        elif guest_id:
            favorite_stores.query.filter(
                and_(favorite_stores.guest_id == guest_id, favorite_stores.uuid == uuid)).delete()

        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)

        db.session.commit()

        return jsonify(status='OK', message='Store deleted')

    @api.doc('get favorite store(s)')
    @use_args(FavoriteSchema())
    def get(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        if user_id:
            stores = favorite_stores.query.filter_by(user_id=user_id).all()
        elif guest_id:
            stores = favorite_stores.query.filter_by(guest_id=guest_id).all()
        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)

        stores, errors = favorite_instance.dump(stores)

        return jsonify(status='OK', stores=stores)
