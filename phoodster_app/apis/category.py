__author__ = 'xuxiaowei'
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import request, jsonify,send_from_directory
import time
from elasticsearch import ElasticsearchException
from sqlalchemy import and_, or_
from .recipes import recipes_at_category_params
from pprint import pprint

api = Namespace('', description='Categories related operations')


@api.route('/categories_counter', methods=['GET'])
class CategoriesCounter(Resource):
    @api.doc('categories counter')
    @use_args({'country': fields.Str(missing='sv'), 'uuid': fields.Str(), 'page': fields.Integer(missing=1),
               'per_page': fields.Integer(missing=30)})
    def get(self, args):
        """
        TODO: Get categories with the number of recipes for each one
        :return:
        """

        country = args['country']
        uuid = args['uuid']
        page = args['page']
        per_page = args['per_page']

        counter = []

        for category_index in range(1, 9):
            try:
                result = recipes_at_category_params(category_index, country, uuid, page, per_page)
            except Exception as e:
                res = {'status': 'error', 'message': e.message}
                return jsonify(res)

            counter.append(len(result))

        res = {'status': 'OK', 'id': uuid, 'payload': {'counter': counter}}

        return jsonify(res)


@api.route('/store_categories', methods=['GET'])
class CategoriesStore(Resource):
    @api.doc('categories for store')
    @use_args({'country': fields.Str(missing='sv'), 'uuid': fields.Str()})
    def get(self, args):
        """
        get the categories of a certain store.
        aggregation is expensive thus a cache strategy is needed in the future to reach a better performance
        :param args: 
        :return:
        """
        country = args['country']
        store_id = args['uuid']

        # Add offers
        if country == 'sv':
            analyzer = 'search_grams'
        else:
            analyzer = 'english_analyzer'

        if not store_id:
            return jsonify(status='BAD_REQUEST', message='The store id is needed')

        offers_query = {
            'filter': {
                'term': {
                    'uuid': store_id
                }
            }
        }

        offers_index = app.config['OFFER_INDEX_MAP'].get(country, 'sweden_offers')
        try:
            result = es.search(index=offers_index, doc_type="offer", body=offers_query)
        except ElasticsearchException:
            return jsonify({'status': 'SERVER_ERROR', 'message': 'Error On Server.'})

        try:
            store = result['hits']['hits'][0]
        except IndexError:
            return jsonify({'status': 'INVALID_REQUEST', 'message': 'Document with the id doesn\'t Exist'})
        else:
            store_items = store['_source']['items']

        offers = [prepare_nested_query(item['title'], _, analyzer) for _, item in enumerate(store_items)]
        query = {
            "size": 0,
            "aggs": {
                "group_by_category": {
                    "terms": {
                        "size": 200,
                        "field": "categories"
                    }
                }
            },
            "query": {
                "bool": {
                    "minimum_should_match": 1,
                    "should": offers
                }
            }
        }

        recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, "sweden_recipes")
        try:
            aggregations = es.search(index=recipes_index, doc_type='recipe', body=query)
        except ElasticsearchException:
            return jsonify({"status": "SERVER_ERROR", "message": "Error On Server."})

        buckets = aggregations['aggregations']['group_by_category']['buckets']
        # MySQL Part
        if country == 'sv':
            results = db.session.query(categories.id, categories.image_name, categories.sort, categories.sv, tags.name). \
                outerjoin(tags, tags.category_id == categories.id). \
                filter(tags.country == country). \
                all()
        else:
            results = db.session.query(categories.id, categories.image_name, categories.sort, categories.usa,
                                       tags.name). \
                outerjoin(tags, tags.category_id == categories.id). \
                filter(tags.country == country). \
                all()


        the_categories = {}
        # key_name = country
        if country == 'sv':
            for result in results:
                if result.sv not in the_categories:
                    the_categories[result.sv] = {}
                if 'tags' in the_categories[result.sv]:
                    the_categories[result.sv]['tags'].append(result.name)
                else:
                    the_categories[result.sv] = {
                        'name': result.sv,
                        'id': result.id,
                        'image_name': result.image_name,
                        'sort': result.sort,
                        'tags': [result.name],
                        'count': 0
                    }

            for key, value in the_categories.iteritems():
                for bucket in buckets:
                    if bucket['key'].lower() in value['tags']:
                        the_categories[key]['count'] += bucket['doc_count']

            return jsonify(status='OK', categories=the_categories)


@api.route('/categories', methods=['GET'])
class Categories(Resource):
    @api.doc('categories')
    @use_args({'country': fields.Str(missing='sv')})
    def get(self, args):
        country = args['country']
        if country == 'sv':
            results = db.session.query(categories.id, categories.image_name, categories.sort, categories.sv, tags.name). \
                outerjoin(tags, categories.id == tags.category_id). \
                filter(tags.country == country). \
                all()
        else:
            results = db.session.query(categories.id, categories.image_name, categories.sort, categories.usa,
                                       tags.name). \
                outerjoin(tags, categories.id == tags.category_id). \
                filter(tags.country == country). \
                all()

        the_categories = {}
        if country == 'sv':

            for result in results:
                if result.sv not in the_categories:
                    the_categories[result.sv] = {}
                if 'tags' in the_categories[result.sv]:
                    the_categories[result.sv]['tags'].append(result.name)
                else:
                    the_categories[result.sv] = {
                        'name': result.sv,
                        'id': result.id,
                        'image_name': result.image_name,
                        'sort': result.sort,
                        'tags': [result.name],
                        'count': 0
                    }

            recipes_index = app.config['RECIPE_INDEX_MAP'].get(country, 'sweden_recipes')
            query = {
                "size": 0,
                "aggs": {
                    "group_by_category": {
                        "terms": {
                            "size": 200,
                            "field": "categories"
                        }
                    }
                }
            }
            aggregations = es.search(index=recipes_index, doc_type='recipe', body=query)
            buckets = aggregations['aggregations']['group_by_category']['buckets']

            for key, value in the_categories.iteritems():
                for bucket in buckets:
                    if bucket['key'].lower() in value['tags']:
                        the_categories[key]['count'] += bucket['doc_count']

            return jsonify(status='OK', categories=the_categories)


"""
Function for the dynamic feeds. It returns the number of categories that should be displayed.
"""


@api.route('/number_categories')
class NumberofCategories(Resource):
    @api.doc('number of categories')
    @use_args({'platform': fields.Str(required=True)})
    def get(self, args):
        platform = args["platform"]

        if platform == "web":
            try:
                response = number_categories.query.filter_by(id=1).first()
                return jsonify(status='OK', num_categories=response.num_web)
            except:
                return jsonify({"status": "Failed", "message": "The database request failed."})

        elif platform == "ios":
            try:
                response = number_categories.query.filter_by(id=1).first()
                return jsonify(status='OK', num_categories=response.num_ios)
            except:
                return jsonify({"status": "Failed", "message": "The database request failed."})

        else:
            return jsonify({"status": "Failed", "message": "Wrong platform - only web and ios is supported."})


@api.route('/categories_images/<string:filename>')
class CategoriesImage(Resource):

    def get(self,filename):
        return send_from_directory('categories_images', filename)