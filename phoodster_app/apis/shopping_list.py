__author__ = 'xuxiaowei'
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import jsonify
import json
from sqlalchemy import and_, or_, func
from elasticsearch import ElasticsearchException

api = Namespace('', description='Shopping list related operations')


########################shopping list#################################################
@api.route('/shopping_lists', methods=['GET','POST'])
class ShoppingLists(Resource):
    @api.doc('get shopping lists')
    @use_args({'guest_id':fields.Str(missing='')})
    def get(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        if user_id:
            results = db.session. \
                query(shopping_lists.id, shopping_lists.name, shopping_lists.description,
                      func.count(ingredients.id).label('ingredients_count')). \
                outerjoin(ingredients, shopping_lists.id == ingredients.shopping_list_id). \
                filter(shopping_lists.user_id == user_id). \
                group_by(shopping_lists.id). \
                all()

        elif guest_id:
            results = db.session. \
                query(shopping_lists.id, shopping_lists.name, shopping_lists.description,
                      func.count(ingredients.id).label('ingredients_count')). \
                outerjoin(ingredients, shopping_lists.id == ingredients.shopping_list_id). \
                filter(shopping_lists.guest_id == guest_id). \
                group_by(shopping_lists.id). \
                all()
        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)

        lists, errors = shopping_lists_instance.dump(results)
        return jsonify(status='OK', lists=lists)


@api.route('/shopping_list', methods=['GET', 'POST', 'DELETE','PUT'])
class ShoppingList(Resource):
    @api.doc('create shopping list')
    @use_args(ShoppingListSchema())
    def post(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        description = args['description']

        name = incremental_table_name('shopping_lists','List',args['name'], user_id, guest_id)
        recipe_uniqid = args['recipe_uniqid']

        if user_id:
            new_shopping_list = shopping_lists(user_id=user_id, name=name, description=description, guest_id=None)
        elif guest_id:
            new_shopping_list = shopping_lists(user_id=None, name=name, description=description, guest_id=guest_id)
        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)

        db.session.add(new_shopping_list)
        db.session.commit()
        list_id = new_shopping_list.id
        if recipe_uniqid:
            new_recipe = recipes_in_list(shopping_list_id=list_id, recipe_uniqid=recipe_uniqid)
            db.session.add(new_recipe)
            db.session.commit()

        if list_id:
            return jsonify(status='OK', list_id=list_id)
        else:
            return jsonify(status='SERVER_ERROR', message='Something went wrong')

    @api.doc('delete shopping list')
    @use_args(ShoppingListSchema())
    def delete(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        _id = args['id']
        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        if user_id:
            shopping_lists.query.filter(and_(shopping_lists.user_id == user_id, shopping_lists.id == _id)).delete()


        elif guest_id:
            shopping_lists.query.filter(and_(shopping_lists.guest_id == guest_id, shopping_lists.id == _id)).delete()

        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)

        recipes_in_list.query.filter_by(shopping_list_id=_id).delete()
        db.session.commit()

        return jsonify(status='OK', message='Shopping list deleted')

    @api.doc('get shopping list')
    @use_args({'id':fields.Integer(required=True)})
    def get(self, args):
        _id = args['id']
        the_list = db.session. \
            query(shopping_lists.id, shopping_lists.name, shopping_lists.description, recipes_in_list.recipe_uniqid,
                  func.count(recipes_in_list.id).label('recipes_count')). \
            outerjoin(recipes_in_list, shopping_lists.id == recipes_in_list.shopping_list_id). \
            filter(shopping_lists.id == _id). \
            group_by(shopping_lists.id). \
            first()
        the_list, errors = shopping_list_instance.dump(the_list)
        if not the_list:
            return jsonify(status='NOT_FOUND_' + _id, lists='The shopping list was not found')

        _ingredients = ingredients.query.filter_by(shopping_list_id=_id).all()
        _ingredients, errors = ingredients_instance.dump(_ingredients)
        the_list['ingredients'] = _ingredients
        uniqid = the_list['recipe_uniqid']

        # # Gets the recipe image path
        results = None
        if uniqid:
            try:
                # Searches in all indexes
                recipes_indexes = ','.join(app.config['RECIPE_INDEX_MAP'].itervalues())
                query = {
                    "_source": {
                        "include": ["credit_text", "images.path"]
                    },
                    "query": {
                        "match": {"uniqid": uniqid}
                    }
                }
                results = es.search(index=recipes_indexes, doc_type='recipe', body=query)
            except ElasticsearchException:
                pass

            if len(results['hits']['hits']):
                source = results['hits']['hits'][0]['_source']
                images = source['images']
                if len(images) > 0:
                    path = images[0]['path']
                    the_list['image_path'] = path
                    the_list['credit_text'] = source['credit_text']

        return jsonify(status='OK', list=the_list)

    @api.doc('edit shopping list')
    @use_args({'name':fields.Str(),'id':fields.Str(),'token':fields.Str()})
    def put(self,args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']
            if guest_id == None:
                return jsonify(status='INVALID_REQUEST', message="Either guest_id or user_id is needed.")

        _id = args['id']
        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        name = args['name']
        if user_id:
            shopping_list=shopping_lists.query.filter(and_(shopping_lists.user_id==user_id,shopping_lists.id==_id)).first()
            shopping_list.name = name
        elif guest_id:
            shopping_list=shopping_lists.query.filter(and_(shopping_lists.guest_id==guest_id,shopping_lists.id==_id)).first()
            shopping_list.name = name
        else:
            return jsonify(status='INVALID_REQUEST', message=INVALID_SESSION_MESSAGE)
        db.session.commit()
        shopping_list,errors = shopping_list_instance.dump(shopping_list)

        return jsonify(status='OK', deck='Shopping list updated')

