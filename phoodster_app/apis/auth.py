# -*- coding: utf-8 -*-

__author__ = 'xuxiaowei'

from phoodster_app import app
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args
from flask import request, jsonify
import time
from elasticsearch import ElasticsearchException
from sqlalchemy import and_, or_
from werkzeug.security import check_password_hash, generate_password_hash
import requests, tinys3
from flask_mail import Message

api = Namespace('', description='Authority related operations')


########################login#################################################

@api.route('/login', methods=['POST'])
class Login(Resource):
    @api.doc('login')
    @use_args(LoginSchema())
    def post(self, args):
        username_or_email = args['username_or_email']
        password = args['password']
        guest_id = args['guest_id']
        if not password or not username_or_email:
            return jsonify(status='INVALID_REQUEST', message='All parameters are required')

        the_user = users.query.filter(or_(users.username == username_or_email, users.email == username_or_email)).first()
        if not the_user:
            return jsonify(status='NOT_FOUND', message='The user/email and/or password does not match with any user')

        the_user,error = user_instance.dump(the_user)
        if check_password_hash(the_user['password_hash'], password):
            del the_user['password_hash']

            if guest_id:
                user_id = the_user['id']
                transfer_guest_items_to_user(guest_id, user_id)
            return jsonify(status='OK', user=the_user)
        else:
            return jsonify(status='NOT_FOUND', message='The user/email and/or password does not match with any user')


########################sign up#################################################
@api.route('/sign_up', methods=['POST'])
class Signup(Resource):
    @api.doc('signup')
    @use_args(SignupSchema())
    def post(self, args):
        username = args['username']
        full_name = args['full_name']
        email = args['email']
        password = args['password']
        facebook_id = args['facebook_id']
        guest_id = args['guest_id']

        # Validates not empty fields
        if not username or not full_name or not email:
            return jsonify(status='INVALID_REQUEST', message='All parameters are required')
        if not password and not facebook_id:
            return jsonify(status='INVALID_REQUEST',
                           message='The password is required if the sign up is not by Facebook')

        try:
            validate_username_email(username, email)
        except Exception as error:
            return jsonify(status='INVALID_REQUEST', message=error.message)

        filename = ''
        if 'photo' in request.files:
            photo = request.files['photo']
            if photo and photo.filename != '' and allowed_file(photo.filename):
                extension = photo.filename.rsplit('.', 1)[1]
                filename = get_random_string(12) + '.' + extension
                # photo.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True,
                                         endpoint=app.config['AWS_ENDPOINT'])
                conn.upload(filename, photo, 'phoodster-user-images', public=True)

        if password:
            password_hash = generate_password_hash(password)
        else:
            password_hash = ''

        token = get_random_string(24)
        new_user = users(username=username, full_name=full_name, email=email, facebook_id=facebook_id,
                         password_hash=password_hash, token=token, photo_path=filename)
        db.session.add(new_user)
        db.session.commit()
        user = users.query.filter_by(token=token).first()

        if user.id:
            if args['guest_id']:
                transfer_guest_items_to_user(args['guest_id'], user.id)

            return jsonify(status='OK', user_id=user.id, photo_path=filename, token=token)
        else:
            return jsonify(status='SERVER_ERROR', message='Something went wrong')


########################login with facebook#################################################

@api.route('/login_with_facebook', methods=['POST'])
class FacebookLogin(Resource):
    @api.doc('Login with facebook')
    @use_args(FacebookLoginSchema())
    def post(self, args):
        """

        :param args: facebook_id,facebook_token,guest_id
        :return:
        """
        facebook_id = args['facebook_id']
        facebook_token = args['facebook_token']
        guest_id = args['guest_id']

        try:
            r = requests.get(
                "https://graph.facebook.com/debug_token?input_token=" + facebook_token + "&access_token=" + app.config[
                    'FACEBOOK_ACCESS_TOKEN']
            )
        except:
            return jsonify(status='FAILED', message='Could not verify the token.')

        try:
            if r.json()["data"]["is_valid"] == False:
                return jsonify(status='FAILED', message='Facebook Access Token is invalid')
        except:
            return jsonify(status='FAILED', message='The token responds was not correct.')

        user = users.query.filter_by(facebook_id=facebook_id).first()
        user = user_instance.dump(user)

        if guest_id:
            user_id = user['id']
            transfer_guest_items_to_user(guest_id, user_id)
        if user:
            return jsonify(status='OK', user=user)
        else:
            return jsonify(status='NOT_FOUND', message='The Facebook user requires sign up')


########################reset password#################################################

@api.route('/reset_password', methods=['POST','GET'])
class ResetPassword(Resource):
    @api.doc('Reset Password')
    @use_args(ResetPasswordSchema())
    def get(self, args):
        """

        :param args: email
        :return:
        """

        email = args['email']
        if not email:
            return jsonify(status='INVALID_REQUEST', message='The email is required')

        new_password = get_random_string(8)
        password_hash = generate_password_hash(new_password)

        user = users.query.filter_by(email=email).first()
        if user:
            user.password_hash = password_hash
            db.session.commit()
            body = "Your new password is " + new_password
            message = Message('Your password in Phoodster was reset',
                              body=body,
                              sender="no-reply@phoodster.com",
                              recipients=[email])
            mail.send(message)

            return jsonify(status='OK', message='A email will send you with the instructions')
        else:
            return jsonify(status='BAD_REQUEST', message='This email is not registered on Phoodster')


########################unlink #################################################
@api.route('/unlink_account', methods=['POST'])
class UnlinkAccount(Resource):
    @api.doc('Unlink Account')
    @use_args(UnlinkAccountSchema())
    def get(self, args):
        """

        :param args: token
        :return:
        """

        try:
            user_id = get_user_id()
        except Exception as error:
            return jsonify(status='INVALID_REQUEST', message=error.message)

        user = users.query.filter_by(id=user_id).first()
        user.facebook_id = None
        db.session.commit()

        return jsonify(status='OK', message='Unlinked Facebook account')


########################change password #################################################

@api.route('/change_password', methods=['POST'])
class changePassword(Resource):
    @api.doc('change password')
    @use_args({'old_password':fields.Str(),'new_password':fields.Str(),'confirm_password':fields.Str()})
    def post(self, args):

        try:
            user_id = get_user_id()
        except Exception as error:
            return jsonify(status='INVALID_REQUEST', message=error.message)

        old_password = args['old_password']
        new_password = args['new_password']
        confirm_password = args['confirm_password']

        user = users.query.filter_by(id=user_id).first()

        if check_password_hash(user.password_hash, old_password):
            if new_password == confirm_password and new_password != '':
                user.password_hash = generate_password_hash(new_password)
                db.session.commit()

                return jsonify(status='ok', message="Password has been changed sucessfully")
            else:
                return jsonify(status='not ok', message="Password confirmation failed,please try again")
        else:
            return jsonify(status='not ok', message='Your password is incorrect,please try again')
