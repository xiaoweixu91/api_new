__author__ = 'xuxiaowei'

from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import jsonify

from sqlalchemy import and_, or_, func

api = Namespace('', description='Deck related operations')


@api.route('/decks', methods=['GET'])
class Decks(Resource):
    @api.doc('Get decks')
    @use_args(DeckSchema())
    def get(self, args):

        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        if user_id:
            results = db.session. \
                query(decks.id, decks.name, decks.description,
                      func.count(recipes.id).label('recipes_count')). \
                outerjoin(recipes, decks.id == recipes.deck_id). \
                filter(decks.user_id == user_id). \
                group_by(decks.id). \
                all()
        elif guest_id:
            results = db.session. \
                query(decks.id, decks.name, decks.description,
                      func.count(recipes.id).label('recipes_count')). \
                outerjoin(recipes, decks.id == recipes.deck_id). \
                filter(decks.guest_id == guest_id). \
                group_by(decks.id). \
                all()
        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)
        results, errors = decks_instance.dump(results)
        if len(results) > 0:
            for idx, result in enumerate(results):
                recipes_results = recipes.query.filter_by(deck_id=result['id']).limit(4)
                recipes_results, errors = deck_recipe_instance.dump(recipes_results)
                result['recipes'] = recipes_results

        return jsonify(status='OK', decks=results)


@api.route('/deck', methods=['GET', 'POST', 'PUT','DELETE'])
class Deck(Resource):
    @api.doc('Get deck')
    @use_args(DeckSchema())
    def get(self, args):
        _id = args['id']

        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        _deck = decks.query.filter_by(id=_id).first()
        _deck, errors = deck_instance.dump(_deck)

        if not _deck:
            return jsonify(status='NOT_FOUND', lists='The decks was not found')

        _recipes = recipes.query.filter_by(deck_id=_id).all()
        _recipes, errors = deck_recipe_instance.dump(_recipes)
        _deck['recipes'] = _recipes

        return jsonify(status='OK', deck=_deck)

    @api.doc('Create new deck')
    @use_args({'guest_id':fields.Str(),'description':fields.Str(missing=''),'name':fields.Str()})
    def post(self,args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        description = args['description']

        if user_id:

            name = incremental_table_name('decks', 'Deck', args['name'], user_id)
            new_deck = decks(name=name,user_id=user_id,description=description)
            db.session.add(new_deck)
        elif guest_id:
            column = 'guest_id'
            identifier = guest_id
            name = incremental_table_name('decks', 'Deck', args['name'], None, guest_id)
            new_deck = decks(name=name,guest_id=guest_id,description=description)
            db.session.add(new_deck)
        else:
            return jsonify(status='SERVER_ERROR', message=INVALID_SESSION_MESSAGE)
        db.session.commit()
        deck_id = new_deck.id

        if deck_id:
            return jsonify(status='OK', deck_id=deck_id)
        else:
            return jsonify(status='SERVER_ERROR', message='Something went wrong')

    @api.doc('Edit deck')
    @use_args(DeckSchema())
    def put(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        _id = args['id']

        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        name = args['name']
        description = args['description']

        # cursor = mysql.connection.cursor()
        if user_id:
            _deck = decks.query.filter(and_(decks.user_id == user_id, decks.id == _id)).first()
            _deck.name = name
            _deck.description = description

        elif guest_id:
            _deck = decks.query.filter(and_(decks.guest_id == guest_id, decks.id == _id)).first()
            _deck.name = name
            _deck.description = description

        else:
            return jsonify(status='INVALID_REQUEST', message=INVALID_SESSION_MESSAGE)

        db.session.commit()
        _deck, errors = deck_instance.dump(_deck)
        return jsonify(status='OK', deck=_deck)

    @api.doc('Delete deck')
    @use_args(DeckSchema())
    def delete(self, args):
        user_id = None
        guest_id = None

        try:
            user_id = get_user_id()
        except:
            guest_id = args['guest_id']

        _id = args['id']
        if not _id:
            return jsonify(status='INVALID_REQUEST', message='The id is required')

        if user_id:
            decks.query.filter(and_(decks.id == _id, decks.user_id == user_id)).delete()
        elif guest_id:
            decks.query.filter(and_(decks.id == _id, decks.guest_id == guest_id)).delete()

        else:
            return jsonify(status='INVALID_REQUEST', message=INVALID_SESSION_MESSAGE)
        db.session.commit()

        return jsonify(status='OK', message='Deck deleted')
