__author__ = 'xuxiaowei'
from phoodster_app import app
from flask_restplus import Namespace, Resource
from ..schemas import *
from .. import es, mail, db
from ..models import *
from ..utils import *
from webargs.flaskparser import use_args, parser, use_kwargs
from flask import request, jsonify, send_file
import time, io

from flask_mail import Message
from elasticsearch import ElasticsearchException
from sqlalchemy import and_, or_, func
import tinys3, StringIO, requests
from werkzeug.security import check_password_hash, generate_password_hash
from PIL import Image
from resizeimage import resizeimage

api = Namespace('', description='User related operations')


@api.route('/modify_category', methods=['POST', 'GET', 'DELETE', 'PUT'])
class tool_category(Resource):
    @api.doc('show category')
    def get(self):
        result = db.session.query(categories.id, categories.sv, tags.name, categories.image_name). \
            outerjoin(tags, categories.id == tags.category_id). \
            all()
        result = [{idx.id: {idx.sv: idx.name}} for idx in result]
        return jsonify(result)

    @api.doc('create category')
    @use_args(CategoriesSchema())
    def post(self, args):
        """
        create category
        :param args:
        :return:
        """
        if requests.files:

            for f in request.files:
                data = request.files[f]
                # print data
                name = data.filename
                name = name.split('@')[0]
                with Image.open(data) as image:
                    # @2x.png
                    cover_2x = resizeimage.resize_thumbnail(image, [828, 412])
                    # @.png
                    cover = resizeimage.resize_thumbnail(image, [414, 206])

                    out = StringIO.StringIO()
                    cover.save(out, 'PNG')
                    # #### AWS
                    conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True,
                                             endpoint=app.config['AWS_ENDPOINT'])
                    conn.upload(name + '.png', out, bucket='phoodster-category-images', public=True,
                                content_type='image')

                    out_2x = StringIO.StringIO()
                    cover_2x.save(out_2x, 'PNG')
                    # #### AWS
                    # conn = tinys3.Connection(AWS_ACCESS, AWS_SECRET, tls=True, endpoint=AWS_ENDPOINT)
                    conn.upload(name + '@2x.png', out_2x, bucket='phoodster-category-images', public=True,
                                content_type='image')
                    conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True,
                                             endpoint=app.config['AWS_ENDPOINT'])
                    conn.upload(name + '@3x.png', data, bucket='phoodster-category-images', public=True,
                                content_type='image')
                    conn.upload("gastrolocal_image/" + name, data, bucket='gastrolocals-data', public=True,
                                content_type='image')

            return 'uploaded'

        else:

            name_sv = args['sv']
            name_usa = args['usa']
            image_name = args['image_name']
            tags = args['tags']
            tags = tags.split(',')
            max_sort = db.session.query(func.max(categories.sort).label('max_sort')). \
                first()[0]
            print max_sort
            input_sort = max_sort + 1
            new_category = categories(name_usa=name_usa, name_sv=name_sv, image_name=image_name, input_sort=input_sort)
            db.session.add(new_category)
            db.session.commit()
            id_cat = new_category.id

            for tag in tags:
                new_tag = tags(name=tag, country='sv', category_id=id_cat)
                db.session.add(new_tag)
            db.session.commit()

            return 'successfully created'

    @api.doc('edit category')
    @use_args({'name_sv_old': fields.Str(), 'name_sv_new': fields.Str()})
    def put(self, args):
        name_sv_old = args['name_sv_old']
        name_sv_new = args['name_sv_new']

        the_category = categories.query.filter_by(sv=name_sv_old).first()
        the_category.sv = name_sv_new
        db.session.commit()

        return 'edited and saved'

    @api.doc('delete category')
    @use_args(CategoriesSchema())
    def delete(self, args):
        name_sv = args['sv']
        the_category = categories.query.filter_by(sv=name_sv).first()
        id_cat = the_category.id
        tags.query.filter(tags.category_id == id_cat).delete()
        categories.filter(categories.id == id_cat).delete()
        db.session.commit()

        return 'successfully deleted'


@api.route('/modify_tag', methods=['POST', 'DELETE'])
class tool_tag(Resource):
    @api.doc('create tag')
    @use_args({'tag_name': fields.Str(), 'tag_language': fields.Str(), 'category_name': fields.Str()})
    def post(self, args):
        tag_name = args['tag_name']
        tag_language = args['tag_language']
        category_name = args['category_name']
        the_category = categories.query.filter_by(sv=category_name).first()
        id_cat = the_category.id
        new_tag = tags(name=tag_name, country=tag_language, category_id=id_cat)
        db.session.add(new_tag)
        db.session.commit()
        return jsonify({"status": 'OK', 'message': "tag added"})

    @api.doc('delete tag')
    @use_args({'tag_name': fields.Str(), 'tag_language': fields.Str(), 'category_name': fields.Str()})
    def delete(self, args):
        tag_name = args['tag_name']
        tag_language = args['tag_language']
        category_name = args['category_name']
        the_category = categories.query.filter_by(sv=category_name).first()
        id_cat = the_category.id
        tags.query.filter(
            and_(tags.name == tag_name, tags.country == tag_language, tags.category_id == id_cat)).delete()
        db.session.commit()
        return jsonify({"status": 'OK', 'message': "tag deleted"})


@api.route('/modify_order', methods=['GET', 'PUT'])
class tool_order(Resource):
    @api.doc('show the order of category')
    def get(self):
        _categories = categories.query.filter().order_by(categories.sort.asc()).all()
        _categories, error = categories_instance.dump(_categories)
        return jsonify(_categories)

    @api.doc('edit the order of category')
    @use_args(CategoriesSchema(many=True))
    def put(self, args):
        for idx, arg in enumerate(args):
            the_category = categories.query.filter_by(sv=arg['sv'])
            the_category.sort = idx
            db.session.commit()

        return jsonify({'status': 'OK','message':'order changed'})
