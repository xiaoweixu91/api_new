# -*- coding: utf-8 -*-

# __author__ = 'xuxiaowei'
import math, re
from . import app

from flask import request
from . import db, es
from .models import *
from sqlalchemy import and_, or_, text
import random, string
import tinys3


# from elasticsearch import ElasticsearchException


# from flask import jsonify
# from . import es
def pager(total, page_number, per_page=25, max_page=5):
    total = min(int(math.ceil(float(total) / per_page)), max_page)
    return dict(current=page_number, total=total)


WORD_MAP = {
    'chicken': 'kycklingfile, kycklingbröst, kycklingklubba, kycklinglår, kycklingvinge, kycklingben, kycklinglever, kalkon, kycklingbröstfile, kycklingfärs, kycklinginnerfilé, kycklinglårfile, svartkycklingklubba, svartkycklingfile, gåslever, anka, majskycklingbröstfile, majskyckling, kalkonbröstfile, hönsbröstfile, kalkonbröst, majskycklingklubba, kalkonfärs, ankbröst, kalkonstek, anklår, pärlhönsbröst, kalkonbacon',
    'fish': 'laxfile, torskfile, sill, sejfile, vitfisk, svärdfisk, tonfisk, alaskan pollock, lax, kallrökt lax, torskryggfile, rödspättafile, tonfiskloins, havskattfile, torksloins, piggvarsfile, gravad lax, makrill, koljafile, bläckfisk, strömming, strömmingfile, ansjovis, hokifile',
    'shellfish': 'räkor, havskräftor, musslor, krabba, hummer, ostron , kräftor, scallops , Vongolemusslor, blåmusslor , king prawns , sniglar',
    'beef': 'oxkind, högrev, entrecôte, ryggbiff, oxfilé, rostbiff, rostasfile, oxsvans, renskav, nötbog, kållapp, hängmörad biff, älgstek, renstek, högrev, hjortstek, biff, kalvbräss, älgfärs, nötstek, nörfärs, luffarbiff, grytbitar, älgskav, vildsvinsfärs, renytterfile, sjömansbiff, pulled beef',
    'pork': 'griskind, fläskkarre, fläskfile, ribs, rostbiff, fläskytterfile, fläskkotlett, rulle, pulled pork, stekfläsk, revbensspjäll, skinkstek, revben, skinka, kassler, kotlettbacon, falukorv, bacon, skinka, gourmetskinka, spare ribs, sidfläsk, fläskfärs,',
    'lamb': 'lammentrecote, lammkorv, lammburgare, lammstek, lamm kabanoss, lammbringa, lammfärs',
    'diary': 'mjölk, grädde, smör, creme fraiche, yoghurt, gräddfil, matyoghurt, matlagningsgrädde, creme fraiche, filmjölk, kvarg, turkisk yoghurt, a-fil, grekisk yoghurt, smetana',
    'cheese': 'mozzarella, prästost, keso, gouda, fetaost, herrgårdsost, hushållsost, riven ost, cheddar, greveost, emmentaler, västerbottenost, halloumiost, färskost, gratängost, brieost, buffelmozzarella, getost, brie, parmesan, borgmästarost, gorgonzola, ädelost, blåmöggelost, tryffelbrie',
    'carbs': 'ris, basmati ris, jasmin ris, råris, vildris, matris, fullkornsris, långkornigt matris, svartris, rödris, avorioris, pasta, penne, rigatoni, tagliatelle, spagetti, ravioli, risoni, spagettini, gnocchi, macaroni, makaroner, snabbmakaroner, fullkornsmakaroner, couscous, fullkornscouscous, bulgur, fullkornsbulgur, potatis, sötpotatis, färskpotatis, vete, korn, havre',
    'nuts': 'jordnötter, soja, cashew, hasselnöt, pekannöt, paranöt, pistagemandel, makadamianöt, queenslandnöt, valnöt, mandel, aprikoskärna, senapsfrö, sesam, solros, vallmo, pinjenöt'
}


def prepare_negative_query(key):
    """

    :param key:
    :return:
    """
    key = key.lower()
    if key not in WORD_MAP:
        return {}

    return {"constant_score": {"filter": {"not": {"query": {"match": {"name": WORD_MAP[key]}}}}}}


INVALID_SESSION_MESSAGE = "Please login again."


def get_user_id():
    token = request.form.get('token')
    if not token:
        token = request.args.get('token')
    if not token:
        params = request.get_json()
        if params:
            token = params['token']

    if not token:
        raise Exception('The token is required')
    if token:

        user = users.query.filter_by(token=token).first()
        # cursor.execute('SELECT id FROM users WHERE token = %s', (token,))
        # result = cursor.fetchone()
        if not user:
            raise Exception(INVALID_SESSION_MESSAGE)
        user_id = user.id
        if user_id:
            return user_id
        else:
            raise Exception(INVALID_SESSION_MESSAGE)
    else:
        raise Exception('The token is required')


def prepare_nested_query(q, d, analyzer):
    """

    :param q:
    :param d:
    :return:
    """

    if re.search('(kyckling)|(fläsk)|(torsk)|(filé)|(côte)|(biff)', q.lower()):
        boost_factor = 0.9
    else:
        boost_factor = 1.0

    return {
        "function_score": {
            "query": {
                "nested": {
                    "path": "ingredients.ingredients",
                    "score_mode": "max",
                    "query": {
                        "match": {
                            "ingredients.ingredients.name": {
                                "query": q,
                                "operator": "and",
                                "analyzer": analyzer
                            }
                        }

                    },
                    "inner_hits": {"name": d}
                }
            },

            "functions": [
                {
                    "script_score": {
                        "script": "_score *" + str(boost_factor)
                    }
                }
            ]
        }
    }


def process_response(store_items, results):
    """

    :param store_items:
    :param recipes:
    :return:
    """
    response = []
    store_items_length = len(store_items)

    for result in results:
        source = result['_source']
        source['index'] = result['_index']
        source['id'] = result['_id']
        ingredients = source['ingredients']
        # ingredient_count = sum(len(_['ingredients']) for _ in ingredients)

        inner_hits = result['inner_hits']

        matches = 0
        inner_matches = set()
        # inner_matches=[]
        for k, hit in inner_hits.iteritems():
            if not hit['hits']['total']:
                continue

            k = int(k)

            for item in hit['hits']['hits']:
                dish_offset = item['_nested']['offset']
                inner_offset = item['_nested']['_nested']['offset']
                inner_matches.add((dish_offset, inner_offset))
                # inner_matches.append((dish_offset, inner_offset))

                inner_source = item['_source']

                if k < store_items_length:
                    inner_source['price'] = store_items[k].get('price', '')
                    inner_source['offer'] = store_items[k]['title']
                    inner_source['description'] = store_items[k].get('description', '')
                    inner_source['Jmfp'] = store_items[k].get('Jmf', '')

                ingredients[dish_offset]['ingredients'][inner_offset] = inner_source

        matches += len(inner_matches)
        res = {'recipe': source, 'matches': matches}
        # res = {'ingredient_count': ingredient_count, 'recipe': source, 'matches': matches}
        response.append(res)
    # response.sort(key=itemgetter('matches'), reverse=True)
    return response


def transfer_guest_items_to_user(guest_id, user_id):
    table_list = [decks, favorite_stores, shopping_lists]
    for table in table_list:
        for i in table.query.filter_by(guest_id=guest_id).all():
            i.user_id = user_id
            i.guest_id = None

    db.session.commit()


def validate_username_email(username, email):
    # Validates if username name email not exists
    user = users.query.filter(or_(users.username == username, users.email == email)).first()
    if user:
        if username == user.username:
            raise Exception("The username '%s' is already in use" % username)
        elif email == user.email:
            raise Exception("The email '%s' is already in use" % email)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ['png', 'jpg', 'jpeg', 'gif']


def get_random_string(length=24):
    simple_chars = string.ascii_letters + string.digits
    return ''.join(random.choice(simple_chars) for i in xrange(length))


"""
Internal function for checking the Username
"""


def internal_check_username_available(new_username):
    response = users.query.with_entities(users.username).filter(users.username == new_username).scalar()
    if response != None:
        return False
    else:
        return True


def incremental_table_name(table, default_name, received_name, user_id, guest_id=None):
    name = received_name

    if not name:
        name = default_name

    if user_id is not None:
        column = 'user_id'
        identifier = user_id
    elif guest_id is not None:
        column = 'guest_id'
        identifier = guest_id
    else:
        return default_name
    # sql = text("SELECT count(id) FROM shopping_lists WHERE :column = :identifier AND name LIKE :name'+'%' +' ORDER BY id DESC LIMIT 0, 1")

    if table == 'shopping_lists':
        result = shopping_lists.query.filter(
            and_(shopping_lists.name == name, shopping_lists.user_id == identifier)).count()
    elif table == 'decks':
        result = decks.query.filter(
            and_(decks.name == name, decks.user_id == identifier)).count()
    # cursor.execute('SELECT count(id) as count FROM '+table+' WHERE '+column+' = %s AND name LIKE %s '
    #                                                                         'ORDER BY id DESC LIMIT 0, 1', (identifier, name + '%'))
    #
    # result = cursor.fetchone()

    if result:
        previous_number = int(result)
        next_number = previous_number + 1
        name = name + " " + str(next_number)
    elif received_name is None:
        name = default_name + " 1"

    return name


def remove_user_photo(user_id):
    the_user = users.query.filter_by(id=user_id).first()

    if the_user and len(the_user.photo_path) > 0:
        # os.remove(os.path.join(app.config['UPLOAD_FOLDER'], result['photo_path']))
        conn = tinys3.Connection(app.config['AWS_ACCESS'], app.config['AWS_SECRET'], tls=True,
                                 endpoint='s3-eu-west-1.amazonaws.com')
        conn.delete(the_user.photo_path, 'phoodster-user-images')
