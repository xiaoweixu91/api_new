__author__ = 'xuxiaowei'
from flask import Blueprint
from flask_restplus import Api

blueprint = Blueprint('api_v1', __name__, url_prefix='')
api_v1 = Api(blueprint, version='1.0', title='api', description='Phoodster')

from .apis.offers import api as offers
from .apis.shopping_list import api as shopping_list
from .apis.favorite import api as favorite
from .apis.deck import api as deck
from .apis.recipes import api as recipes
from .apis.category import api as category
from .apis.gastrolocal import api as gastrolocal
from .apis.user import api as user
from .apis.tag_tool import api as tag_tool
from .apis.auth import api as auth
from .apis.ingredient import api as ingredient

api_v1.add_namespace(offers)
api_v1.add_namespace(shopping_list)
api_v1.add_namespace(favorite)
api_v1.add_namespace(deck)
api_v1.add_namespace(recipes)
api_v1.add_namespace(category)
api_v1.add_namespace(gastrolocal)
api_v1.add_namespace(user)
api_v1.add_namespace(tag_tool)
api_v1.add_namespace(auth)
api_v1.add_namespace(ingredient)

