__author__ = 'xuxiaowei'

from . import db
from sqlalchemy.dialects.mysql import TINYINT


class store_search_location(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    date = db.Column(db.DateTime)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)


class users(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    username = db.Column(db.VARCHAR(255))
    full_name = db.Column(db.VARCHAR(255))
    email = db.Column(db.VARCHAR(255))
    verified = db.Column(TINYINT(1))
    bio = db.Column(db.VARCHAR(2048))
    photo_path = db.Column(db.VARCHAR(255))
    password_hash = db.Column(db.VARCHAR(255))
    token = db.Column(db.VARCHAR(255))
    facebook_id = db.Column(db.VARCHAR(255))
    location = db.Column(db.VARCHAR(1024))





class favorite_stores(db.Model):
    internal_id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    id = db.Column(db.VARCHAR(255))
    name = db.Column(db.VARCHAR(255))
    address = db.Column(db.VARCHAR(255))
    uuid = db.Column(db.VARCHAR(255))
    user_id = db.Column(db.Integer, unique=True)
    location = db.Column(db.VARCHAR(255))
    guest_id = db.Column(db.VARCHAR(255))


class tags(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    name = db.Column(db.VARCHAR(255))
    country = db.Column(db.Enum('sv', 'usa'))
    category_id = db.Column(db.Integer, unique=True)


class recipes(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    name = db.Column(db.VARCHAR(255))
    credit = db.Column(db.VARCHAR(255))
    image_path = db.Column(db.VARCHAR(255))
    uniqid = db.Column(db.VARCHAR(255))
    country = db.Column(db.Enum('sv', 'usa'))
    image_source_url = db.Column(db.VARCHAR(255))
    deck_id = db.Column(db.Integer, unique=True)


class decks(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    name = db.Column(db.VARCHAR(255))
    description = db.Column(db.VARCHAR(255))
    image_path = db.Column(db.VARCHAR(255))
    guest_id = db.Column(db.VARCHAR(255))
    user_id = db.Column(db.Integer, unique=True)


class blogger_information(db.Model):
    BloggerId = db.Column(db.VARCHAR(10), primary_key=True)
    BloggerName = db.Column(db.CHAR(45))
    token = db.Column(db.VARCHAR(10))
    Description = db.Column(db.Text)
    Subtitle = db.Column(db.VARCHAR(50))
    pic_path = db.Column(db.VARCHAR(50))
    big_pic_path = db.Column(db.VARCHAR(50))


class shopping_lists(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    user_id = db.Column(db.Integer, unique=True)
    name = db.Column(db.VARCHAR(255))
    description = db.Column(db.VARCHAR(255))
    guest_id = db.Column(db.VARCHAR(255))


class recipes_in_list(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    shopping_list_id = db.Column(db.Integer, unique=True)
    recipe_uniqid = db.Column(db.VARCHAR(255))


    # class user_preference(db.Model):
    #     id = db.Column(db.Integer,primary_key=True)
    #     cooking_skill = db.Column(db.Enum(''))


class ingredients(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    name = db.Column(db.VARCHAR(255))
    description = db.Column(db.VARCHAR(2048))
    price = db.Column(db.VARCHAR(255))
    measure = db.Column(db.VARCHAR(255))
    shopping_list_id = db.Column(db.Integer, unique=True)
    done = db.Column(TINYINT)


class categories(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    usa = db.Column(db.VARCHAR(255))
    sv = db.Column(db.VARCHAR(255))
    image_name = db.Column(db.VARCHAR(255))
    sort = db.Column(db.Integer)

class most_used_ingredients(db.Model):
    ingredient_name = db.Column(db.VARCHAR(255),primary_key=True)
    count = db.Column(db.Integer)

class number_categories(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True, unique=True)
    num_ios = db.Column(db.Integer)
    num_web = db.Column(db.Integer)

