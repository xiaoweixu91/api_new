from flask import Flask, Blueprint, abort
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from elasticsearch import Elasticsearch
from flask_mail import Mail
from webargs.flaskparser import parser
# from .views.offers import api_v1 as offers
from flask_restplus import Api
import urllib3


application = app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config.default')
# app.config.from_pyfile('config.py')

db = SQLAlchemy(app)

es = Elasticsearch(app.config['ES_DB'], verify_certs=False)
mail = Mail(app)
urllib3.disable_warnings()
cors = CORS(app, resources={r"/*": {"origins": "*"}})

@parser.error_handler
def handle_request_parsing_error(err):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    abort(422, errors=err.messages)

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response

from phoodster_app.apiv1 import blueprint as api_v1
app.register_blueprint(api_v1)

