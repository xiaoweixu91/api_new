__author__ = 'xuxiaowei'
from flask import Flask, Blueprint
from flask_restplus import Api

blueprint = Blueprint('api_v2', __name__, url_prefix='')
api_v2 = Api(blueprint, version='2.0', title='api', description='Phoodster')

from .apis.offers import api as offers

api_v2.add_namespace(offers)
