__author__ = 'xuxiaowei'
from marshmallow import Schema, fields


class StoresSchema(Schema):
    lat = fields.Float(missing=59.33)
    lng = fields.Float(missing=18.05)
    page = fields.Int(missing=1)
    q = fields.Str(missing='')
    token = fields.Str(missing='')
    size = fields.Int(missing=20)

    class Meta:
        strict = True


class StoreSchema(Schema):
    uuid = fields.Str(required=True)
    lat = fields.Str(missing='')
    lng = fields.Str(missing='')
    country = fields.Str(required=True, missing='sv')
    token = fields.Str(missing='')

    class Meta:
        strict = True


###################################
class RecipesSchema(Schema):
    q = fields.Str(missing='')
    page = fields.Int(missing=1)
    country = fields.Str(missing='sv')
    size = fields.Int( missing=30)
    category = fields.Int(missing=-1)
    uuid = fields.Str(missing='')

    class Meta:
        strict = True


class RecipeSchema(Schema):
    uniqid = fields.Str(required=True)
    recipeID = fields.Str(missing='')
    search_index = fields.Str()
    token = fields.Str(missing='')
    guest_id = fields.Str(missing='')
    country = fields.Str(required=True, missing='sv')

    class Meta:
        strict = True


class OffersSchema(Schema):
    uuid = fields.Str(required=True)
    country = fields.Str(required=True, missing='sv')

    class Meta:
        strict = True


class StoreRecipeSchema(Schema):
    recipeID = fields.Str(required=True)
    storeID = fields.Str(required=True)
    token = fields.Str(missing='')

    country = fields.Str(required=True, missing='sv')

    class Meta:
        strict = True


class GasRecipesSchema(Schema):
    gastrolocalID = fields.Str(required=True)

    class Meta:
        strict = True


class blogger_informationSchema(Schema):
    BloggerId = fields.Str(required=True)
    BloggerName = fields.Str(required=True)
    token = fields.Str(required=True)
    Description = fields.Str(required=True)
    Subtitle = fields.Str(required=True)
    pic_path = fields.Str(required=True)
    big_pic_path = fields.Str(required=True)


blogger_information_instance = blogger_informationSchema()
bloggers_information_instance = blogger_informationSchema(many=True)


class UploadRecipeSchema(Schema):
    name = fields.Str(required=True)
    short_description = fields.Str(missing='')
    servings = fields.Str(missing='')
    ingredients = fields.List(fields.Dict())
    steps = fields.List(fields.Str(missing=''))
    prep_time = fields.Str(missing='0 minute')
    cook_time = fields.Str(missing='0 minute')
    token = fields.Str(required=True)
    tag = fields.List(fields.Str(missing=''))
    uniqid = fields.Str(required=True)
    num_ingredients=fields.Integer(missing=0)
    class Meta:
        strict = True


class LoginSchema(Schema):
    username_or_email = fields.Str(required=True)
    password = fields.Str(required=True)
    guest_id = fields.Str(missing='')

    class Meta:
        strict = True


class FacebookLoginSchema(Schema):
    facebook_id = fields.Str(required=True)
    facebook_token = fields.Str(required=True)
    guest_id = fields.Str(missing='')


class SignupSchema(Schema):
    username = fields.Str(required=True)
    full_name = fields.Str(required=True)
    email = fields.Str(required=True)
    password = fields.Str(required=True)
    facebook_id = fields.Str(missing='')
    guest_id = fields.Str(missing='')

    class Meta:
        strict = True


class UserSchema(Schema):
    id = fields.Integer()
    username = fields.Str()
    full_name = fields.Str()
    email = fields.Str()
    verified = fields.Boolean()
    bio = fields.Str()
    photo_path = fields.Str()
    password_hash = fields.Str()
    token = fields.Str()
    facebook_id = fields.Str()
    location = fields.Str()


user_instance = UserSchema()


class ProfileSchema(Schema):
    full_name = fields.Str()
    bio = fields.Str()
    username = fields.Str(missing='')
    token = fields.Str()
    password = fields.Str(missing='')
    photo_path = fields.Str(missing='')

    class Meta:
        strict = True


class ResetPasswordSchema(Schema):
    email = fields.Str(required=True)

    class Meta:
        strict = True


class FeedbackSchema(Schema):
    message = fields.Str(required=True)
    token = fields.Str(required=True)

    class Meta:
        strict = True


class UnlinkAccountSchema(Schema):
    token = fields.Str(required=True)

    class Meta:
        strict = True


class ChangePasswordSchema(Schema):
    token = fields.Str()

    class Meta:
        strict = True


class Preference(Schema):
    cooking_skill = fields.Str()
    time_to_cook = fields.Str()
    diets = fields.Str()
    allergy = fields.Str()

    class Meta:
        strict = True


class ShoppingListSchema(Schema):
    id = fields.Str()
    token = fields.Str()
    guest_id = fields.Str(missing='')
    description = fields.Str(missing='')
    name = fields.Str()
    recipe_uniqid = fields.Str(missing='')
    ingredients_count = fields.Integer(dump_only=True)
    recipes_count = fields.Integer(dump_only=True)

    class Meta:
        strict = True


shopping_lists_instance = ShoppingListSchema(many=True)
shopping_list_instance = ShoppingListSchema()


class FavoriteSchema(Schema):
    token = fields.Str()
    id = fields.Str()
    name = fields.Str()
    address = fields.Str()
    uuid = fields.Str()
    location = fields.Str(missing='')

    guest_id = fields.Str(missing='')

    class Meta:
        strict = True


favorite_instance = FavoriteSchema(many=True)


class DeckSchema(Schema):
    id = fields.Str()
    name = fields.Str()
    description = fields.Str(missing='')
    token = fields.Str()
    image_path = fields.Str()
    guest_id = fields.Str(missing='')
    recipes_count = fields.Integer(dump_only=True)

    class Meta:
        strict = True


decks_instance = DeckSchema(many=True)
deck_instance = DeckSchema()


class IngredientsSchema(Schema):
    token = fields.Str()
    id = fields.Str()
    name = fields.Str()
    description = fields.Str()
    price = fields.Str()
    measure = fields.Str()
    shopping_list_id = fields.Integer()
    done = fields.Boolean()
    class Meta:
        strict = True

ingredients_instance = IngredientsSchema(many=True)


class IngredientSchema(IngredientsSchema):
    token = fields.Str()
    shopping_list_name = fields.Str(missing='')
    shopping_list_description = fields.Str(missing='')
    recipe_uniqid = fields.Str(missing='')

    guest_id = fields.Str(missing='')
    class Meta:
        strict = True

class DeckRecipeSchema(RecipeSchema):
    token = fields.Str()
    id = fields.Str()
    name = fields.Str()
    credit = fields.Str()
    image_path = fields.Str(missing='')
    image_source_url = fields.Str()
    deck_id = fields.Str(missing='')
    deck_name = fields.Str(missing='')
    deck_description = fields.Str(missing='')
    class Meta:
        strict = True

deck_recipe_instance = DeckRecipeSchema(many=True)


class MostUsedIngredientsSchema(Schema):

    ingredient_name = fields.Str()
    count = fields.Integer()
    class Meta:
        strict = True

most_used_ingredients_instance = MostUsedIngredientsSchema(many=True)


class CategoriesSchema(Schema):
    id = fields.Integer()
    sv = fields.Str()
    usa = fields.Str()
    image_name = fields.Str()
    sort = fields.Integer()
    tags = fields.Str()
    class Meta:
        strict = True
categories_instance = CategoriesSchema(many=True)
