# -*- coding: utf-8 -*-

__author__ = 'xuxiaowei'
import os
from phoodster_app import app
from phoodster_app.apiv1 import blueprint as api_v1
from phoodster_app.apiv2 import blueprint as api_v2

import unittest
from flask_restplus import Api
from flask_testing import LiveServerTestCase
import json, tinys3
from flask import jsonify
from elasticsearch import Elasticsearch
from elasticsearch.exceptions import ElasticsearchException
import StringIO

# api_v1 = Api(blueprint, version='1.0', title='api', description='Phoodster')

app.register_blueprint(api_v1)
# app.register_blueprint(api_v2)

# S3 Config
AWS_ACCESS = "AKIAITQWJRBULVOBIGTA"
AWS_SECRET = "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"
AWS_ENDPOINT = "s3-eu-west-1.amazonaws.com"

# ES Config
ES_DB = os.getenv('ES_DB', 'NA')

if ES_DB == "NA":
    es = Elasticsearch()
else:
    es = Elasticsearch([ES_DB], verify_certs=False)


class FlaskTestCase(unittest.TestCase):
    def setUp(self):
        # propagate the exceptions to the test client

        app.testing = True
        # create a test client
        self.app = app.test_client(self)

    def tearDown(self):
        pass

        # def test_fetchStores_without_query_v1(self):
        #     response = self.app.get("/stores?lat=59.33&lng=18.05&q=&page=1&token=", content_type='application/json')
        #
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     print data
        # self.assertEqual(data['status'],'OK')


        # def test_fetchStores_with_token(self):
        #
        #     response = self.app.get("/stores?lat=59.33&lng=18.05&q=&page=1&token=xfz6DPTEQGRpfuenGpTGaKi4",content_type = 'application/json')
        #
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     # print data
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchStores_with_query(self):
        #     response = self.app.get("/stores?lat=59.33&lng=18.05&q=solna&page=1",content_type = 'application/json')
        #
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     # print data
        #     self.assertEqual(data['status'],'OK')
        # #
        # def test_fetchRecipes_with_query(self):
        #     response = self.app.get('/random_recipes?size=30&q=taco&page=1')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchRecipes_without_query(self):
        #     response = self.app.get('/random_recipes?size=30&q=&page=1')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchRecipes_with_category(self):
        #     response = self.app.get('/recipes_category?category=5&page=1')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        # # #
        # def test_fetchRecipes_with_category_in_store(self):
        #     response = self.app.get('/recipes_category?category=5&page=1&uuid=e18becaa2da1c84349f63e82f534112a74bf49f8')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')

        # def test_fetchOffers(self):
        #     response = self.app.get('/store_offers?uuid=e18becaa2da1c84349f63e82f534112a74bf49f8')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchRecipe(self):
        #     response = self.app.get('/recipe?uniqid=tva-smarriga-wraps-med-rostbiff-koket')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchRecipe_with_token(self):
        #     response = self.app.get('/recipe?uniqid=cowboygryta-mittkok&token=Svxd0dnEexjCAmijVE8XauqF')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        # #
        # def test_fetchRecipesForStore_with_category(self):
        #     response = self.app.get('/recipes_category_store?uuid=e18becaa2da1c84349f63e82f534112a74bf49f8&category=1&page=1')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        # #
        # def test_fetchRecipesForStore_without_query(self):
        #     response = self.app.get('recipes_store?uuid=e18becaa2da1c84349f63e82f534112a74bf49f8&page=1&q=')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        #
        # def test_fetchRecipesForStore_with_query(self):
        #     response = self.app.get('/api/1.0/recipes_store?uuid=e18becaa2da1c84349f63e82f534112a74bf49f8&page=1&q=taco')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        # #
        # def test_fetchStore_without_cord(self):
        #     response = self.app.get('store_detail?uuid=e18becaa2da1c84349f63e82f534112a74bf49f8&lat=&lng=')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchStore_with_token(self):
        #     response = self.app.get('/store_detail?uuid=e18becaa2da1c84349f63e82f534112a74bf49f8&lat=&lng=&token=xfz6DPTEQGRpfuenGpTGaKi4')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     # print data
        #     self.assertEqual(data['status'],'OK')
        #
        # #
        # def test_fetchRecipeForStore(self):
        #     response = self.app.get('/recipe_store?storeID=e18becaa2da1c84349f63e82f534112a74bf49f8&recipeID=blt-burgare-mittkok')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchSimilarRecipes(self):
        #     response = self.app.get('/similar_recipes?search_index=sweden_recipes_mittkok&uniqid=AVa3aXKtIZRkoxwlSpB4')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')
        #
        # def test_fetchGastrolocalRecipes(self):
        #     response = self.app.get('/gastrolocal_recipes?gastrolocalID=1')
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     self.assertEqual(data['status'],'OK')

        # def test_checkToken_success(self):
        #     data={"token":"qpz23"}
        #     # response = self.app.post('/checktoken',data = data,follow_redirects=True,headers=[('X-Requested-With', 'XMLHttpRequest')])
        #     response = self.app.post('/check_token',data = json.dumps(data),content_type='application/json')
        #     # print response.data
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     # print data
        #     self.assertEqual(data['status'],1)
        #
        # def test_checkToken_fail(self):
        #     data={"token":"qpz2"}
        #     # response = self.app.post('/checktoken',data = data,follow_redirects=True,headers=[('X-Requested-With', 'XMLHttpRequest')])
        #     response = self.app.post('/check_token',data = json.dumps(data),content_type='application/json')
        #     # print response.data
        #     self.assertEqual(response.status_code, 200)
        #     data = json.loads(response.data)
        #     # print data
        #     self.assertNotEqual(data['status'],1)
        # def test_uploadRecipe(self):
        #     data = {
        #         "name": 'title',
        #         "short_description": 'desc',
        #         "servings": 1,
        #         "ingredients": [{"dish": '', "ingredients": [{"name": "", "quantity": "", "unit": ""}] }],
        #         "steps": ["","",""],
        #         "prep_time": 1,
        #         "cook_time": 2,
        #         "token": 'qpz23',
        #         "tag": ['breakfast'],
        #         "uniqid": "testing",
        # }
        #
        #     response = self.app.post('/upload_recipe',data = json.dumps(data),content_type='application/json')

        # data = json.loads(response.data)
        # print data
        #
        # self.assertEqual(data,'ok')
        # if data == 'ok':
        #
        #     conn = tinys3.Connection(AWS_ACCESS, AWS_SECRET, tls=True, endpoint=AWS_ENDPOINT)
        #     conn.delete('recipes/testing.json',bucket='gastrolocals-data')
        #
        #
        #     search = es.search(
        #
        #         body={
        #             "query": {
        #                 "match": {'uniqid': 'testing'}
        #             }
        #         },
        #         index="sweden_recipes_upload_recipe",
        #         doc_type='recipe',
        #         _source=True,
        #
        #         search_type="scan",
        #         scroll='5m',
        #     )
        #     bulk = ""
        #
        #     try:
        #         scroll_results = es.scroll(scroll_id=search['_scroll_id'], scroll='5m', )
        #         for result in scroll_results['hits']['hits']:
        #             bulk = bulk + '{ "delete" : { "_index" : "' + str(result['_index']) + '", "_type" : "' + str(
        #                 result['_type']) + '", "_id" : "' + str(result['_id']) + '" } }\n'
        #         # Finally do the deleting.
        #         if bulk:
        #             es.bulk(body=bulk)
        #         #Clear the scroll id
        #         es.clear_scroll(scroll_id=search['_scroll_id'])
        #         # Since scroll throws an error catch it and break the loop.
        #     except ElasticsearchException:
        #         pass

        #
        # def test_uploadImage(self):
        #     with open('testing.png') as image:
        #         response = self.app.post('/api/1.0/upload_image',
        #                                  content_type='multipart/form-data',
        #                                  data=dict(
        #                                      {'userpic[]': (image, 'testing.png')},
        #
        #                                  ), follow_redirects=True
        #                                  )
        #
        #     if response.data == 'ok':
        #         conn = tinys3.Connection(AWS_ACCESS, AWS_SECRET, tls=True, endpoint=AWS_ENDPOINT)
        #         conn.delete("recipe-images/testing.png", bucket='gastrolocals-data')

        # def test_login(self):
        #     # pass
        #     response = self.app.post('/login',
        #                              content_type='multipart/form-data',
        #                              data=dict(
        #                                  {'username_or_email': 'xiaowei.xu@phoodster.com',
        #                                   'password': '123',
        #                                   'guest_id': '28B32BF4-4DB2-4DDC-966C-FDC57FF516B8'}))
        #
        #     self.assertEqual(json.loads(response.data)['status'], 'OK')
        #
        # def test_signup_invalid(self):
        #     # pass
        #     response = self.app.post('/sign_up',
        #                              content_type='multipart/form-data',
        #                              data=dict(
        #                                  {'username': 'xiaowei',
        #                                   'full_name': 'xiaoweixu',
        #                                   'email': 'xiaowei.xu@phoodster.com',
        #                                   'password': '123'
        #                                   }))
        #
        #
        #     self.assertEqual(json.loads(response.data)['status'],'INVALID_REQUEST')

        # def test_edit_profile(self):
        #     data = {
        #         'username':'xiaowei.xu@phoodster.com',
        #         'token':'xfz6DPTEQGRpfuenGpTGaKi4',
        #         'full_name':'Xiaowei Xu',
        #         'bio':'new api',
        #         'photo_path':'ukzstwPlA8Kl.jpg',
        #         'password':'123'
        #     }
        #     response = self.app.post('/profile',
        #                              content_type='application/json',
        #                              data=json.dumps(data)
        #                              )

        # def test_reset_password(self):
        #     data = {
        #         'email':'xiaowei@phoodster.com',
        #
        #     }
        #     response = self.app.post('/reset_password',
        #                              content_type='application/json',
        #                              data=json.dumps(data)
        #                              )

        # def test_send_feedback(self):
        #     data = {
        #         'token': 'xfz6DPTEQGRpfuenGpTGaKi4',
        #         'message':'good app bro, good app'
        #
        #     }
        #     response = self.app.post('/send_feedback',
        #                              content_type='multipart/form-data',
        #                              data=dict(data)
        #                              )

    # def test_create_shopping_list(self):
    #     data = {
    #         'token': 'xfz6DPTEQGRpfuenGpTGaKi4',
    #         'description':'good app',
    #         'name':'test_api',
    #         'recipe_uniqid':'blt-burgare-mittkok'
    #
    #     }
    #     response = self.app.post('/shopping_list',
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )

    # def test_delete_shopping_list(self):
    #     data = {
    #         'token': 'xfz6DPTEQGRpfuenGpTGaKi4',
    #         'id':113
    #     }
    #     response = self.app.delete('/shopping_list',
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    # def test_create_favorite_store(self):
    #     data = {
    #         'token': 'xfz6DPTEQGRpfuenGpTGaKi4',
    #         'id':'AVgZmye7VpN05UehaW3i',
    #         'name':'Coop Konsum Råsundavägen',
    #         'address':'Råsundavägen 163',
    #         'uuid':'522c2305aafda17087b9b0793514e7c7c901c15e',
    #         'location':'59.36474, 17.98168'
    #     }
    #     response = self.app.post('/favorites',
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    # def test_remove_favorite_store(self):
    #     data = {
    #         'token': 'xfz6DPTEQGRpfuenGpTGaKi4',
    #         'uuid':'522c2305aafda17087b9b0793514e7c7c901c15e'
    #     }
    #     response = self.app.delete('/favorites',
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    # def test_get_favorite_store(self):
    #     # data = {
    #     #     'token': 'xfz6DPTEQGRpfuenGpTGaKi4'
    #     # }
    #     response = self.app.get('/favorites?token=xfz6DPTEQGRpfuenGpTGaKi4',
    #                              content_type='application/json'
    #                              # content_type='multipart/form-data',
    #                              # data=dict(data)
    #                              )

    # def test_get_shopping_lists(self):
    #     response = self.app.get('/shopping_lists?token=xfz6DPTEQGRpfuenGpTGaKi4',
    #                              content_type='application/json'
    #                              # content_type='multipart/form-data',
    #                              # data=dict(data)
    #                              )
    #     print response.data
    #
    # def test_get_shopping_list(self):
    #     response = self.app.get('/shopping_list?id=99',
    #                              content_type='application/json'
    #                              # content_type='multipart/form-data',
    #                              # data=dict(data)
    #                              )
    #     print response.data
    # def test_edit_shopping_list(self):
    #     data = {
    #         'id': 99,
    #         'name':'test',
    #         'description':'testest',
    #         'token':'xfz6DPTEQGRpfuenGpTGaKi4'
    #     }
    #
    #     response = self.app.put('/shopping_list',
    #                              # content_type='application/json'
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    #     print response.data
    # #

    # def test_get_decks(self):
    #     response = self.app.get('/deck?token=xfz6DPTEQGRpfuenGpTGaKi4',
    #                              content_type='application/json'
    #                              # content_type='multipart/form-data',
    #                              # data=dict(data)
    #                              )
    #     print response.data
    #
    # def test_get_deck(self):
    #     data = {
    #         'id': 55
    #     }
    #     response = self.app.get('/deck',
    #                              # content_type='application/json'
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    #     print response.data

    # def test_edit_deck(self):
    #     data = {
    #         'id': 55,
    #         'name':'test',
    #         'description':'testest',
    #         'token':'Svxd0dnEexjCAmijVE8XauqF'
    #     }
    #     response = self.app.put('/deck',
    #                              # content_type='application/json'
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    #     print response.data
    # def test_get_recipes_of_shopping_list(self):
    #     data = {
    #         'id': 99
    #     }
    #     response = self.app.get('/recipes_at_shopping_list',
    #                              # content_type='application/json'
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    #     print response.data

    # def test_delete_recipe_of_deck(self):
    #     data = {
    #         'id': 79,
    #         'token':'Svxd0dnEexjCAmijVE8XauqF'
    #     }
    #     response = self.app.delete('/recipe_at_deck',
    #                              # content_type='application/json'
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    #     print response.data
    #
    # def test_create_recipe_of_deck(self):
    #     data = {
    #         'id': 79,
    #         'token':'Svxd0dnEexjCAmijVE8XauqF'
    #     }
    #     response = self.app.delete('/recipe_at_deck',
    #                              # content_type='application/json'
    #                              content_type='multipart/form-data',
    #                              data=dict(data)
    #                              )
    #     print response.data


    # def test_favorite_recipes_uniqids(self):
    #
    #     response = self.app.get('/favorite_recipes_uniqids?token=Svxd0dnEexjCAmijVE8XauqF',
    #                              content_type='application/json'
    #                              # content_type='multipart/form-data',
    #                              # data=dict(data)
    #                              )
    #     print response.data

    # def test_store_categories(self):
    #     data={
    #         'country':'sv',
    #         'uuid':'522c2305aafda17087b9b0793514e7c7c901c15e'
    #     }
    #
    #     response = self.app.get('/categories_store',
    #                              content_type='application/json',
    #                              # content_type='multipart/form-data',
    #                              data=json.dumps(data)
    #                              )
    #     print response.data
    # def test_fetch_gastrolocals(self):
    #
    #
    #     response = self.app.get('/gastrolocal'
    #                              # content_type='application/json',
    #                              # content_type='multipart/form-data',
    #                              # data=json.dumps(data)
    #                              )
    #     print response.data

    # def test_delete_user(self):
    #
    #     response = self.app.delete('/profile?token=olvxRtMSOdQUbXBYRC68Astm',
    #                                )
    #     print response.data

    # def test_create_category(self):
    #
    #     data = {
    #         'sv':'testa',
    #         'usa':'test',
    #         'image_name':'test_image',
    #         'tags':'test_tag'
    #     }
    #
    #     response = self.app.post('/modify_category',
    #                              content_type='application/json',
    #                              data=json.dumps(data)
    #                              )
    #     print response.data

    # def test_delete_category(self):
    #
    #     data = {
    #         'sv':'test_sv',
    #
    #     }
    #
    #     response = self.app.delete('/modify_category',
    #                              content_type='application/json',
    #                              data=json.dumps(data)
    #                              )
    #     print response.data

    # def test_show_category(self):
    #
    #
    #     response = self.app.get('/modify_category'
    #                              )
    #     print response.data
    #
    # def test_show_order(self):
    #     response = self.app.get('/modify_order')
    #     print response.data

    def test_edit_order(self):
        data = [
            {
                "id": 5,
                "image_name": "MainDishes",
                "sort": 0,
                "sv": "Vardagsmat",
                "usa": "Everyday food"
            },
            {
                "id": 8,
                "image_name": "Vegetarian",
                "sort": 1,
                "sv": "Veggie",
                "usa": "Veggie heaven"
            },
            {
                "id": 1,
                "image_name": "Breakfast&Brunch",
                "sort": 2,
                "sv": "Godmorgon!",
                "usa": "Good morning!"
            },
            {
                "id": 4,
                "image_name": "Appetizers",
                "sort": 3,
                "sv": "Mingla",
                "usa": "Ready to mingle"
            },
            {
                "id": 7,
                "image_name": "Drinks",
                "sort": 4,
                "sv": "Vatten \u00e4r tr\u00e5kigt",
                "usa": "Water is boring"
            },
            {
                "id": 6,
                "image_name": "Desserts",
                "sort": 5,
                "sv": "Sockerkick",
                "usa": "Sugar bomb!"
            },
            {
                "id": 3,
                "image_name": "LCHFFriendly",
                "sort": 6,
                "sv": "LCHF Friendly",
                "usa": "LCHF Friendly"
            },
            {
                "id": 2,
                "image_name": "Vegan",
                "sort": 7,
                "sv": "Vegan",
                "usa": "Vegan"
            },
            {
                "id": 29,
                "image_name": "sanna",
                "sort": 8,
                "sv": "test_sv",
                "usa": "test_us"
            },
            {
                "id": 30,
                "image_name": "new_year",
                "sort": 99,
                "sv": "2017",
                "usa": "new years"
            }
        ]
        response = self.app.put('/modify_order',
                                content_type='application/json',
                                data=json.dumps(data)
                                )
        print response.data


suite = unittest.TestLoader().loadTestsFromTestCase(FlaskTestCase)
unittest.TextTestRunner(verbosity=2).run(suite)

# if __name__ =='__main__':
#     unittest.main()
